function getServerHttp(path){
    var domain = "http://zweixin.77bi.vip";
    return domain + path;
}
//获取地址栏参数值
function getUrlParam(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); // 构造一个含有目标参数的正则表达式对象
	var r = window.location.search.substr(1).match(reg); // 匹配目标参数
	if (r != null){
		return decodeURIComponent(r[2]);
	}
	return null; //返回参数值
}
//url后面加参数
function combineParam(url,param,val){
    if(url.indexOf(param +"=") > 0 ){
        return url;
    }
    var ch = '?';
    if(url.indexOf('?') >0 ){
        ch = '&';
    }
    return url + ch + param + '=' + val;
}
//净化分享url
function getCleanUrl(){
    var url = window.location.href;
    // 1. 去掉code参数
    var code = getUrlParam("code");
    url = url.replace("code="+code ,"");
    url = url.replace("?&" ,"?");
    url = url.replace("&&" ,"&");
    return url;
}
function wxLogin(){
    var url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=@appid&redirect_uri=@redirect_uri&response_type=code&scope=snsapi_userinfo&state=123#wechat_redirect";
    url = url.replace("@appid",jsConfig.appId);
    url = url.replace("@redirect_uri",encodeURI(window.location.href));
    window.location.href=url;
}
function wxLogin2(){
    $.ajax({
        type : "GET",
        url : getServerHttp("/zweixin") + "/weixin/api/user/getUserInfo",
        data : {
        	code : getUrlParam("code")
        },
        dateType : 'json',
        success : function(result) {
        	if(result.code == 200){
                localStorage.setItem("is_login","true");
                localStorage.setItem("wxUser",JSON.stringify(result.result));
                combineLine();
                checkIsSubscribe();
                console.log(getShareData());
                wx.ready(function () {
                    refreshShare(getShareData());
                });
        	}else{
        	}
        }
    });
}

//两个openid连线
function combineLine(){
    var wxUser = JSON.parse(localStorage.getItem("wxUser"));
    var openid = wxUser.openid;
    var originOpenid = localStorage.getItem("originOpenid");

    $.ajax({
        type : "GET",
        url : getServerHttp("/zweixin") + "/weixin/api/user/line/combineLine",
        data : {
            originOpenid : originOpenid,
        	openid : openid
        },
        dateType : 'json',
        success : function(result) {
        	if(result.code == 200){
                console.log("连线成功.");
        	}else{
        	}
        }
    });
}
function checkIsSubscribe(){
    var wxUser = JSON.parse(localStorage.getItem("wxUser"));
    var openid = wxUser.openid;
    $.ajax({
        type : "GET",
        url : getServerHttp("/zweixin") + "/weixin/api/user/getUserInfoBack",
        data : {
        	openid : openid
        },
        dateType : 'json',
        success : function(result) {
        	if(result.code == 200){
                var userInfo = result.result;
                if(!userInfo.subscribe){
                    setTimeout(() => {
                        toSubscribe();
                    }, 5000);
                }
        	}else{
        	}
        }
    });
}
function toSubscribe(){
    layer.open({
        content: '和我们一起点亮灯塔...'
        ,btn: ['确定']
        ,yes: function(index){
            window.location.href = "./ercode.html";
        }
      });
}
//检测并设置源openid
function setOriginOpenid(){
    var urlOriginOpenid = getUrlParam("originOpenid");
    if(!urlOriginOpenid){
        return;
    }
    var originOpenid = localStorage.getItem("originOpenid");
    if(originOpenid){
        return;
    }
    localStorage.setItem("originOpenid",urlOriginOpenid);
}
//加载数量
function loadCount(){
    var countConfig = {
        count1:1623,
        count2:6090
    };
    $.ajax({
        type : "GET",
        url : getServerHttp("/zweixin") + "/weixin/api/user/getUserCount",
        dateType : 'json',
        async: false,
        success : function(result) {
            if(result.code == 200){
                countConfig = result.result;
            }else{
            }
        }
    });
    return countConfig;
}
function getShareData(){
    var shareURL = getCleanUrl(shareURL);
    var wxUserStr = localStorage.getItem("wxUser");
    console.log(wxUserStr);
    if(wxUserStr){
        var wxUser = JSON.parse(wxUserStr);
        var openid = wxUser.openid;
        shareURL = combineParam(shareURL,"originOpenid",openid);
    }
    var shareData = {
        title:"众志成城,"+countConfig.count1 + "人一起祈福",
        desc: "与"+countConfig.count1+"一起,为"+countConfig.count2+"名新冠肺炎者祈福......",
        link: shareURL,
        imgUrl: "http://zweixin.77bi.vip/zweixin-wx/resource/img/lazhu.png"
    };
    return shareData;
}