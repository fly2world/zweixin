var isTest = false;
function loadJsConfig() {
    var jsConfig = {};
    $.ajax({
        type: "GET",
        url: getServerHttp("/zweixin") + "/weixin/api/js/getConfig",
        data: {
            test: isTest
        },
        dateType: 'json',
        async: false,
        success: function (result) {
            jsConfig = JSON.parse(result);
        }
    });
    return jsConfig;
}
var jsConfig = loadJsConfig();
console.log(jsConfig);
console.log(jsConfig.appId);
wx.config({
    debug: isTest,
    appId: jsConfig.appId,
    timestamp: jsConfig._wxTimestamp,
    nonceStr: jsConfig._wxNoncestr,
    signature: jsConfig._wxSignature,
    jsApiList: [
        'checkJsApi',
        'onMenuShareTimeline',
        'onMenuShareAppMessage',
        'onMenuShareQQ',
        'onMenuShareWeibo',
        'hideMenuItems',
        'showMenuItems',
        'hideAllNonBaseMenuItem',
        'showAllNonBaseMenuItem',
        'translateVoice',
        'startRecord',
        'stopRecord',
        'onRecordEnd',
        'playVoice',
        'pauseVoice',
        'stopVoice',
        'uploadVoice',
        'downloadVoice',
        'chooseImage',
        'previewImage',
        'uploadImage',
        'downloadImage',
        'getNetworkType',
        'openLocation',
        'getLocation',
        'hideOptionMenu',
        'showOptionMenu',
        'closeWindow',
        'scanQRCode',
        'chooseWXPay',
        'openProductSpecificView',
        'addCard',
        'chooseCard',
        'openCard'
    ]
});
if (isTest == true) {
    wx.error(function (res) {
        console.log(res.errMsg);
    });
}
// <%-- 默认分享数据 --%>
console.log(typeof (shareData));
var shareData = typeof (shareData) === 'undefined' ? {
    title: '微信JS-SDK Demo测试',
    desc: '微信JS-SDK,帮助第三方为用户提供更优质的移动web服务',
    link: 'http://www.eqbidding.com/',
    imgUrl: 'http://www.jfinal.com/img/weixin_142X142.jpg'
} : shareData;
function refreshShare(shareData){
    wx.onMenuShareAppMessage(shareData);
    wx.onMenuShareTimeline(shareData);
    // wx.onMenuShareWeibo(shareData);
    // wx.onMenuShareQQ(shareData);
}
// <%--新版微信--%>
wx.ready(function () {
    refreshShare(shareData);
});
