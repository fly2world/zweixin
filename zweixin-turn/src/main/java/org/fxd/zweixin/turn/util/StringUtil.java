package org.fxd.zweixin.turn.util;

import java.io.UnsupportedEncodingException;

public class StringUtil {

	public static String convert(String old) {
		try {
			return new String(old.getBytes("utf-8"), "gbk");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String convertGbk(String old) {
		try {
			return new String(old.getBytes("gbk"), "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}
}
