package org.fxd.zweixin.turn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZweixinTurnApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZweixinTurnApplication.class, args);
	}

}
