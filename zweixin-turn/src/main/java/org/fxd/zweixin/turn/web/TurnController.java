package org.fxd.zweixin.turn.web;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.fxd.zweixin.turn.util.HttpUtil;
import org.fxd.zweixin.turn.util.StringUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TurnController {

	public static final String SEND_URL = "http://zweixin.77bi.vip/zweixin/weixin/api/push";

	@PostMapping("/push")
	public String push(String system, String telephone, String first, String keyword1, String keyword2, String keyword3,
			String keyword4, String keyword5, String remark) {
		System.err.println(String.format("%s-->%s-->%s", telephone, first, keyword1));
		Map<String, Object> map = new HashMap<>();
		map.put("system", system);
		map.put("telephone", telephone);
		map.put("first", first);
		map.put("keyword1", keyword1);
		map.put("keyword2", keyword2);
		map.put("keyword3", keyword3);
		map.put("keyword4", keyword4);
		map.put("keyword5", keyword5);
		map.put("remark", remark);
		String body = HttpUtil.post(SEND_URL, map);
		return body;
	}

	@PostMapping("/push-gbk")
	public String push_gbk(String system, String telephone, String content) throws UnsupportedEncodingException {
		System.err.println(String.format("base64-->%s-->%s-->%s", system, telephone, content));
		system = new String(Base64.getDecoder().decode(system.getBytes()), "gbk");
		content = new String(Base64.getDecoder().decode(content.getBytes()), "gbk");
		System.err.println(String.format("%s-->%s-->%s", system, telephone, content));

		Map<String, Object> map = new HashMap<>();
		map.put("system", system);
		map.put("telephone", telephone);
		map.put("content", content);
		String body = HttpUtil.post(SEND_URL, map);
		return body;
	}

	@PostMapping("/push-utf8")
	public String push_utf8(String system, String telephone, String content) {
		System.err.println(String.format("base64-->%s-->%s-->%s", system, telephone, content));
		system = StringUtil.convert(system);
		content = StringUtil.convert(content);
		System.err.println(String.format("%s-->%s-->%s", system, telephone, content));

		Map<String, Object> map = new HashMap<>();
		map.put("system", system);
		map.put("telephone", telephone);
		map.put("content", content);
		String body = HttpUtil.post(SEND_URL, map);
		return body;
	}

	public static void main(String[] args) throws UnsupportedEncodingException {
		String s = "suLK1LT6wuu/y8Dvy7m24L27wfe/z7XCu/k=";
		byte[] ss = Base64.getDecoder().decode(s);
		System.err.println(new String(ss, "gbk"));
	}
}
