import request from '@/router/axios';
import { baseUrl } from '@/config/env';
export const loginByUsername = (uname, password) => request({
    url: baseUrl + '/zweixin/login?uname=' + uname + '&password=' + password,
    method: 'get',
    meta: {
        isToken: false
    }
})

export const getUserInfo = () => request({
    url: '/user/getUserInfo',
    method: 'get'
});

export const refeshToken = () => request({
    url: '/user/refesh',
    method: 'post'
})

// export const getMenu = (type = 0) => request({
//     url: '/user/getMenu',
//     method: 'get',
//     data: {
//         type
//     }
// });

export const getMenu = (type = '0') => request({
    url: baseUrl + '/zweixin/menu',
    method: 'get',
    data: {
        type
    }
});

export const getTopMenu = () => request({
    url: '/user/getTopMenu',
    method: 'get'
});

export const sendLogs = (list) => request({
    url: '/user/logout',
    method: 'post',
    data: list
})

export const logout = () => request({
    url: '/user/logout',
    meta: {
        isToken: false
    },
    method: 'get'
})