package org.fxd.zweixin.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger配置
 * 
 * @author 冯晓东 398479251@qq.com
 * 
 *         swagger-ui.html
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket login_api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.basePackage("org.fxd.zweixin.busi_login")).paths(allowPaths()).build()
				.groupName("登录接口");
	}

	@Bean
	public Docket system_api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.basePackage("org.fxd.zweixin.busi_system")).paths(allowPaths()).build()
				.groupName("系统接口");
	}

	@Bean
	public Docket common_api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.basePackage("org.fxd.zweixin.busi_common")).paths(allowPaths()).build()
				.groupName("公共接口");
	}

	@Bean
	public Docket backletter_api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.basePackage("org.fxd.zweixin.busi_wx")).paths(allowPaths()).build()
				.groupName("信息发送接口");
	}

	/**
	 * 自定义API文档基本信息实体
	 * 
	 * @return
	 */
	private ApiInfo apiInfo() {
		// 构建联系实体，在UI界面会显示
		Contact contact = new Contact("信息发送平台", "http://zweixin.77bi.vip/zweixin/swagger-ui.html", null);
		return new ApiInfoBuilder().contact(contact)
				// 文档标题
				.title("信息发送平台")
				// 文档描述
				.description("信息发送平台API文档")
				// 文档版本
				.version("1.0.0").build();
	}

	/**
	 * path匹配规则
	 * 
	 * @return
	 */
	private Predicate<String> allowPaths() {
		return PathSelectors.any();
	}

}