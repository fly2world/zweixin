package org.fxd.zweixin.config;

/**
 * Web全局配置
 * 
 * @author 冯晓东 398479251@qq.com
 *
 */
public class WebConfig {

	/**
	 * 长度需要>8
	 */
	public static final String DESKEY = "des4MichaelAdminId";
	/**
	 * 记录 cookie中的session值
	 */
	public static final String ADMINSESSION = "ADMINSESSION";

	public static final String SHOPADMINSESSION = "SHOPADMINSESSION";

	public static final String DEPARTMENTSESSION = "DEPARTMENTSESSION";

	public static final String USERSESSION = "USERSESSION";

	public static final String APPSESSION = "APPSESSION";

}
