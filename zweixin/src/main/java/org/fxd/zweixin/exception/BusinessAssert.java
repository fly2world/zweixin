package org.fxd.zweixin.exception;

import org.tmsps.ne4spring.utils.ChkTools;

/**
 * 业务检测 Assert,抛出的异常都是 BusinessException
 * 
 * @author 冯晓东 398479251@qq.com
 *
 */
public class BusinessAssert {

	/**
	 * 为null
	 */
	public static void notNull(Object object, ErrorCodeEnum codeEnum, Object... args) {
		if (object != null) {
			throw new BusinessException(codeEnum, args);
		}
	}

	/**
	 * 不为null
	 */
	public static void isNull(Object object, ErrorCodeEnum codeEnum, Object... args) {
		if (ChkTools.isNull(object) || "undefined".equals(object)) {
			throw new BusinessException(codeEnum, args);
		}
	}

	/**
	 * 为false
	 */
	public static void notTrue(boolean flag, ErrorCodeEnum codeEnum, Object... args) {
		if (!flag) {
			throw new BusinessException(codeEnum, args);
		}
	}

	/**
	 * 为true
	 */
	public static void isTrue(boolean flag, ErrorCodeEnum codeEnum, Object... args) {
		if (flag) {
			throw new BusinessException(codeEnum, args);
		}
	}

	/**
	 * TODO 抛异常
	 * 
	 * @param codeEnum
	 * @param args
	 */
	public static void throwError(ErrorCodeEnum codeEnum, Object... args) {
		throw new BusinessException(codeEnum, args);
	}

}
