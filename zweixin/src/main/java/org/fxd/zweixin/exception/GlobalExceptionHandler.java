package org.fxd.zweixin.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.tmsps.ne4spring.utils.wrapper.WrapMapper;
import org.tmsps.ne4spring.utils.wrapper.Wrapper;

/**
 * 全局的的异常拦截器
 *
 * @author 冯晓东 398479251@qq.com
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

	private Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * 参数非法异常.
	 *
	 * @return the wrapper
	 */
	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseBody
	public Wrapper<String> illegalArgumentException(IllegalArgumentException e) {
		log.error("参数非法异常={}", e.getMessage(), e);
		return WrapMapper.wrap(ErrorCodeEnum.GL99990100.code(), e.getMessage());
	}

	/**
	 * 参数非法异常.
	 *
	 * @return the wrapper
	 */
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseBody
	public Wrapper<String> methodArgumentNotValidException(MethodArgumentNotValidException e) {
		log.error("参数非法异常={}", e.getMessage(), e);
		return WrapMapper.wrap(ErrorCodeEnum.GL99990100.code(), e.getMessage());
	}

	/**
	 * 业务异常.
	 *
	 * @return the wrapper
	 */
	@ExceptionHandler(BusinessException.class)
	@ResponseBody
	public Wrapper<String> businessException(BusinessException e) {
		log.error("业务异常={}", e.getMessage(), e);
		return WrapMapper.wrap(e.getCode() == 0 ? Wrapper.ERROR_CODE : e.getCode(), e.getMessage());
	}

	/**
	 * 全局异常.
	 *
	 * @return the wrapper
	 */
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public Wrapper<String> exception(Exception e) {
		System.err.println(e.getClass());
		log.info("保存全局异常信息 ex={}", e.getMessage(), e);
		return WrapMapper.error(e.getMessage());
	}
}
