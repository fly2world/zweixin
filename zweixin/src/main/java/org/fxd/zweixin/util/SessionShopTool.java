package org.fxd.zweixin.util;

import org.fxd.zweixin.config.WebConfig;
import org.tmsps.ne4spring.utils.ChkUtil;
import org.tmsps.ne4spring.utils.DesUtil;
import org.tmsps.ne4spring.utils.WebUtil;

/**
 * Session业务相关工具类
 * 
 * @author 冯晓东 398479251@qq.com
 *
 */
public class SessionShopTool {

	private static final String SPLIT = ":";

	/**
	 * 设置 or 取消设置 登录key
	 * 
	 * @param sessionKey
	 */
	public static void setSessionShopLoginKey(String shopId, String shopAdminId) {
		String sessionKey = DesUtil.encrypt(WebConfig.SHOPADMINSESSION + SPLIT + shopAdminId + SPLIT + shopId,
				WebConfig.DESKEY);
		CookieUtil.setCookie(WebUtil.getReponse(), WebConfig.SHOPADMINSESSION, sessionKey, 1 * 24 * 60 * 60);
	}

	public static String getSessionShopAdminId() {
		String sessionKey = CookieUtil.getCookie(WebUtil.getRequest(), WebConfig.SHOPADMINSESSION);
		if (ChkUtil.isNull(sessionKey)) {
			return null;
		} else {
			String key = DesUtil.decrypt(sessionKey, WebConfig.DESKEY);
			if (!key.startsWith(WebConfig.SHOPADMINSESSION + SPLIT)) {
				return null;
			}
			String adminId = key.split(SPLIT)[1];
			return adminId;
		}

	}

	public static String getSessionShopId() {
		String sessionKey = CookieUtil.getCookie(WebUtil.getRequest(), WebConfig.SHOPADMINSESSION);
		if (ChkUtil.isNull(sessionKey)) {
			return null;
		} else {
			String key = DesUtil.decrypt(sessionKey, WebConfig.DESKEY);
			if (!key.startsWith(WebConfig.SHOPADMINSESSION + SPLIT)) {
				return null;
			}
			String shopId = key.split(SPLIT)[2];
			return shopId;
		}

	}
}
