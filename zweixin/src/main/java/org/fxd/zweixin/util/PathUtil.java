package org.fxd.zweixin.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PathUtil {

	public static String filePath;

	public String getFilePath() {
		return filePath;
	}

	  @Autowired(required = true)
	  public void setBasePath(@Value("${zweixin.filepath}")String filePath) {
		  PathUtil.filePath = filePath;
	  }
	
}
