package org.fxd.zweixin.util;

import org.fxd.zweixin.config.WebConfig;
import org.tmsps.ne4spring.utils.ChkUtil;
import org.tmsps.ne4spring.utils.DesUtil;
import org.tmsps.ne4spring.utils.WebUtil;

/**
 * Session业务相关工具类
 * 
 * @author 冯晓东 398479251@qq.com
 *
 */
public class SessionUserTool {

	private static final String SPLIT = ":";

	/**
	 * 设置 or 取消设置 登录key
	 * 
	 * @param sessionKey
	 * @return
	 */
	public static String setSessionLoginKey(String userId, String appid) {
		String onekey = DesUtil.encrypt(WebConfig.USERSESSION + SPLIT + userId + SPLIT + appid, WebConfig.DESKEY);
		// CookieUtil.setCookie(WebUtil.getReponse(), WebConfig.USERSESSION, sessionKey,
		// 1 * 24 * 60 * 60);
		return onekey;
	}

	public static String getSessionUserId() {
		String onekey = WebUtil.getRequest().getHeader("onekey");
		if (ChkUtil.isNull(onekey)) {
			return null;
		} else {
			String key = DesUtil.decrypt(onekey, WebConfig.DESKEY);
			if (!key.startsWith(WebConfig.USERSESSION + SPLIT)) {
				return null;
			}
			String userId = key.split(SPLIT)[1];
			return userId;
		}

	}

	public static String getSessionAppId() {
		String onekey = WebUtil.getRequest().getHeader("onekey");
		if (ChkUtil.isNull(onekey)) {
			return null;
		} else {
			String key = DesUtil.decrypt(onekey, WebConfig.DESKEY);
			if (!key.startsWith(WebConfig.USERSESSION + SPLIT)) {
				return null;
			}
			String appId = key.split(SPLIT)[2];
			return appId;
		}

	}

}
