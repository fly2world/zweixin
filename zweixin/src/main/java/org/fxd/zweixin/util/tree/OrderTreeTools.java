package org.fxd.zweixin.util.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author 冯晓东 398479251@qq.com
 *
 */
public class OrderTreeTools {


	@SuppressWarnings("unchecked")
	public static List<Map<String, Object>> turnListToTree(List<Map<String,Object>> menuList) {
		// 转换List为树形结构
		List<Map<String, Object>> nodeList = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < menuList.size(); i++) {
			Map<String,Object> node1 = menuList.get(i);
			String node1_parent_code = (String) node1.get("order_no");

			boolean mark = false;
			for (int j = 0; j < menuList.size(); j++) {
				Map<String, Object> node2 = menuList.get(j);
				String node2_code = (String) node2.get("order_no");
				if (node1_parent_code.equals(node2_code)) {
					System.out.println(i+">>>>>>>>>>..");
					mark = true;
					if (node2.get("children") == null) {
						node2.put("children", new ArrayList<Map<String, Object>>());
					}
					System.out.println("???????????");
					((List<Map<String, Object>>) node2.get("children")).add(node1);

					break;
				}
			}
			if (!mark) {
				nodeList.add(node1);
			}
		}
		return nodeList;
	}

}
