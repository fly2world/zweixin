package org.fxd.zweixin.util.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class TreeTools {

	/**
	 * 预处理树节点
	 * 
	 * @param menuList
	 * @param isChecked
	 * @return
	 */
	private static JSONArray handleTree(JSONArray menuList, boolean isChecked) {
		for (int i = 0; i < menuList.size(); i++) {
			JSONObject map = menuList.getJSONObject(i);
			map.put("key", map.getString("code"));
			map.put("value", map.getString("code"));
			map.put("title", map.getString("name"));
			map.put("text", map.getString("name"));
			map.put("iconCls", map.getString("url"));
			map.put("leaf", true);
			if (isChecked) {
				map.put("checked", false);
			}
		}
		return menuList;
	}

	public static List<Map<String, Object>> turnListToTree(JSONArray menuList) {
		// 转换List为树形结构
		return turnListToTree(menuList, false);
	}

	@SuppressWarnings("unchecked")
	public static List<Map<String, Object>> turnListToTree(JSONArray menuList, boolean isChecked) {
		// 转换List为树形结构
		menuList = handleTree(menuList, isChecked);

		List<Map<String, Object>> nodeList = new ArrayList<Map<String, Object>>();

		for (int i = 0; i < menuList.size(); i++) {
			JSONObject node1 = menuList.getJSONObject(i);
			String node1_code = (String) node1.get("code");
			String node1_parent_code = node1_code.substring(0, node1_code.length() - 3);

			boolean mark = false;
			for (int j = 0; j < menuList.size(); j++) {
				Map<String, Object> node2 = menuList.getJSONObject(j);
				String node2_code = (String) node2.get("code");
				if (node1_parent_code != null && node1_parent_code.equals(node2_code)) {
					mark = true;
					if (node2.get("children") == null) {
						node2.put("children", new ArrayList<Map<String, Object>>());
					}
					((List<Map<String, Object>>) node2.get("children")).add(node1);
					node2.put("leaf", false);
					if (!isChecked) {
						node2.put("expanded", false);
					}
					break;
				}

			}
			if (!mark) {
				nodeList.add(node1);
			}
		}
		return nodeList;
	}

	// 预处理树节点
	public static List<Map<String, Object>> handleTreeForAntd(List<Map<String, Object>> list) {
		for (Map<String, Object> map : list) {
			map.put("id", map.get("kid"));
			map.put("name", map.get("name"));
			map.put("label", map.get("name"));
			map.put("key", map.get("code"));
			map.put("value", map.get("code"));
			map.put("title", map.get("name"));
			map.put("leaf", true);
		}
		return list;
	}

	public static List<Map<String, Object>> turnListToTreeForAntd(List<Map<String, Object>> list) {
		// TODO antd value为code的值
		list = handleTreeForAntd(list);
		return turnTreeList(list);
	}

	@SuppressWarnings("unchecked")
	private static List<Map<String, Object>> turnTreeList(List<Map<String, Object>> list) {
		// TODO 根据部门列表code值，转换为树结构
		List<Map<String, Object>> nodeList = new ArrayList<Map<String, Object>>();
		for (Map<String, Object> node1 : list) {
			String node1_code = (String) node1.get("code");
			String node1_parent_code = node1_code.substring(0, node1_code.length() - 3);
			node1.put("children", new ArrayList<Map<String, Object>>());
			
			boolean mark = false;
			for (Map<String, Object> node2 : list) {
				String node2_code = (String) node2.get("code");

				if (node1_parent_code != null && node1_parent_code.equals(node2_code)) {
					mark = true;
					if (node2.get("children") == null) {
						node2.put("children", new ArrayList<Map<String, Object>>());
					}
					((List<Map<String, Object>>) node2.get("children")).add(node1);
					break;
				}
			}
			if (!mark) {
				nodeList.add(node1);
			}
		}
		return nodeList;
	}

	// 预处理树节点
	public static List<Map<String, Object>> handleValKidTreeForAntd(List<Map<String, Object>> list) {
		for (Map<String, Object> map : list) {
			map.put("id", map.get("kid"));
			map.put("name", map.get("name"));
			map.put("label", map.get("name"));
			map.put("key", map.get("kid"));
			map.put("value", map.get("kid"));
			map.put("title", map.get("name"));
		}
		return list;
	}

	

	public static List<Map<String, Object>> turnValueKidListToTreeForAntd(List<Map<String, Object>> list) {
		// TODO antd value为kid的值
		list = handleValKidTreeForAntd(list);
		return turnTreeList(list);
	}

	public static List<Map<String, Object>> turnKidListToTreeForAntd(List<Map<String, Object>> list) {
		// TODO antd value为kid的值, 通过super_id 关联
		list = handleValKidTreeForAntd(list);
		return turnKidTreeList(list);
	}

	public static List<Map<String, Object>> turnKidListToAuthLeftTreeForAntd(List<Map<String, Object>> list) {
		// TODO antd value为kid的值, 通过super_id 关联
		return turnKidTreeList(list);
	}

	@SuppressWarnings("unchecked")
	private static List<Map<String, Object>> turnKidTreeList(List<Map<String, Object>> list) {
		// TODO 根据super_id，转换为树结构
		List<Map<String, Object>> nodeList = new ArrayList<Map<String, Object>>();
		for (Map<String, Object> node1 : list) {
			String parent_kid = (String) node1.get("super_id");
			boolean mark = false;
			for (Map<String, Object> node2 : list) {
				String super_id = (String) node2.get("kid");

				if (parent_kid != null && parent_kid.equals(super_id)) {
					mark = true;
					if (node2.get("children") == null) {
						node2.put("children", new ArrayList<Map<String, Object>>());
					}
					((List<Map<String, Object>>) node2.get("children")).add(node1);
					break;
				}
			}
			if (!mark) {
				nodeList.add(node1);
			}
		}
		return nodeList;
	}

}
