package org.fxd.zweixin.util.sms;

import java.util.Base64;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.tmsps.ne4spring.utils.JsonUtil;
import org.tmsps.ne4spring.utils.http.HttpBodyTools;

public class SmsUtil {

	public static String send(String mobile, String content) {
		String url = "http://112.35.1.155:1992/sms/norsubmit";
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("ecName", "晋城农村商业银行股份有限公司");
		map.put("apId", "fengxd");
		map.put("secretKey", "jincheng");
		map.put("mobiles", mobile);
		map.put("content", content);
		map.put("sign", "x766mb7pC");
		map.put("addSerial", "");

		// ecName，apId，secretKey，mobiles，content ，sign，addSerial
		StringBuilder mac = new StringBuilder("");
		Collection<Object> values = map.values();
		for (Object object : values) {
			mac.append(object.toString());
		}
		map.put("mac", MD5Util.MD5(mac.toString()).toLowerCase());
		String json = JsonUtil.toJson(map);
		String reqText = Base64.getEncoder().encodeToString(json.getBytes());

		String body = HttpBodyTools.post(url, reqText);
		return body;

	}

	public static void main(String[] args) {
		SmsUtil.send("18649325921", "测试短信");
	}

}
