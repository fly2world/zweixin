package org.fxd.zweixin.util.json;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.tmsps.ne4spring.orm.model.DataModel;
import org.tmsps.ne4spring.utils.ChkTools;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class JsonTools {

	/**
	 * json 字符串 转 Map
	 * 
	 * @param json
	 * @return
	 */
	public static Map<String, Object> jsonStrToMap(String json) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (ChkTools.isNull(json)) {
			return map;
		}

		JSONObject jsonObject = JSON.parseObject(json);
		Set<String> keys = jsonObject.keySet();
		for (String key : keys) {
			map.put(key, jsonObject.get(key));
		}

		return map;
	}

	/**
	 * json 字符串 转 JSONObject
	 * 
	 * @param json
	 * @return
	 */
	public static Object jsonStrToJsonObject(String json, Class<?> clazz) {
		if (clazz == String.class) {
			if (json.length() > 2) {
				// 去掉首尾的引号
				json = json.substring(1, json.length() - 1);
			}
			return json;
		} else {
			JSONObject parse = JSON.parseObject(json);
			return JSON.toJavaObject(parse, clazz);
		}
	}

	/**
	 * 对象-->json
	 * 
	 * @param obj
	 * @return
	 */
	public static String toJson(Object obj) {
		if(obj!=null){
			return JSON.toJSONString(obj).replace("\\n", "").replace("\\t", "").replace("\\r", "");
		}
		return JSON.toJSONString(obj);
	}
	public static String toJson1(Object obj) {
		if (obj != null && obj instanceof DataModel) {
			JSONObject jo = (JSONObject) JSON.toJSON(obj);
			Set<String> keys = jo.keySet();
			keys.forEach(k -> {
				Object v = jo.get(k);
				if (v != null && v.getClass() == Long.class) {
					if (v.toString().length() == 18) {
						jo.put(k, v.toString());
					}
				}
			});
			return jo.toJSONString();
		} else {
			return JSON.toJSONString(obj);
		}
	}

	public static Map<String, Object> objToMap(Object obj) {
		String json = toJson(obj);
		Map<String, Object> map = jsonStrToMap(json);
		return map;
	}

	public static void main(String[] args) {
		String json = "{ \"firstName\": \"Brett\", \"lastName\":\"McLaughlin\", \"email\": \"aaaa\" }";
		Map<String, Object> map = jsonStrToMap(json);
		System.err.println(map);
	}

	public static JSONObject jsonStrToJsonObject(String json) {
		JSONObject parse = JSON.parseObject(json);
		return parse;
	}

	public static JSONArray jsonStrToJsonArray(String json) {
		JSONArray array = new JSONArray();
		if (ChkTools.isNull(json)) {
			return array;
		}

		array = JSON.parseArray(json);

		return array;

	}

}
