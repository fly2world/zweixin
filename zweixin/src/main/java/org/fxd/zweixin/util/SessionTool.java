package org.fxd.zweixin.util;

import org.fxd.zweixin.config.WebConfig;
import org.tmsps.ne4spring.utils.ChkUtil;
import org.tmsps.ne4spring.utils.DesUtil;
import org.tmsps.ne4spring.utils.WebUtil;

/**
 * Session业务相关工具类
 * 
 * @author 冯晓东 398479251@qq.com
 *
 */
public class SessionTool {

	private static final String SPLIT = ":";

	/**
	 * 设置 or 取消设置 登录key
	 * 
	 * @param sessionKey
	 * @return 
	 */
	public static String setSessionLoginKey(String adminId) {
		String sessionKey = DesUtil.encrypt(WebConfig.ADMINSESSION + SPLIT + adminId, WebConfig.DESKEY);
		CookieUtil.setCookie(WebUtil.getReponse(), WebConfig.ADMINSESSION, sessionKey, 1 * 24 * 60 * 60);
		return sessionKey;
	}

	public static String getSessionUserId() {
		String sessionKey = CookieUtil.getCookie(WebUtil.getRequest(), WebConfig.ADMINSESSION);
		if (ChkUtil.isNull(sessionKey)) {
			return null;
		} else {
			String key = DesUtil.decrypt(sessionKey, WebConfig.DESKEY);
			if (!key.startsWith(WebConfig.ADMINSESSION + SPLIT)) {
				return null;
			}
			String adminId = key.substring((WebConfig.ADMINSESSION + SPLIT).length());
			return adminId;
		}

	}

}
