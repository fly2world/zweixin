package org.fxd.zweixin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * 启动类
 *
 */
@SpringBootApplication(scanBasePackages = { "org.tmsps.ne4spring", "com.jfinal", "net.dreamlu.weixin", "org.fxd.zweixin" })
@EnableCaching
public class ZweixinApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZweixinApplication.class, args);
	}
}
