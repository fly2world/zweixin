package org.fxd.zweixin.busi_common.service;

import org.springframework.stereotype.Service;
import org.tmsps.ne4spring.base.BaseService;
import org.tmsps.ne4spring.utils.ChkUtil;

@Service
public class CheckUniqueService extends BaseService {

	public boolean selectTableFindValue(String table, String field, String value) {
		if (ChkUtil.isNull(table) || table.contains(" ")) {
			return false;
		}

		String sql = "select count(*) cnt from @table t where t.status!=-100 and t.@field=?";
		sql = sql.replace("@table", table);
		sql = sql.replace("@field", field);
		Number l = (Number) bs.findMap(sql, value).get("cnt");
		return l.intValue() > 0 ? true : false;
	}

	public boolean selectTableFindValueNotme(String table, String field, String value, String kid) {
		if (ChkUtil.isNull(table) || table.contains(" ")) {
			return false;
		}

		String sql = "select count(*) cnt from @table t where t.status!=-100 and t.@field=? and t.kid!=?";
		sql = sql.replace("@table", table);
		sql = sql.replace("@field", field);
		Number l = (Number) bs.findMap(sql, value, kid).get("cnt");

		return l.intValue() > 0 ? true : false;
	}

}
