package org.fxd.zweixin.busi_common.web;

import org.fxd.zweixin.busi_common.service.CheckUniqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tmsps.ne4spring.base.BaseAction;
import org.tmsps.ne4spring.utils.wrapper.WrapMapper;
import org.tmsps.ne4spring.utils.wrapper.Wrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("唯一性校验")
@RestController
@RequestMapping("/common")
public class CheckUniqueAction extends BaseAction {

	@Autowired
	private CheckUniqueService checkUniqueService;

	@ApiOperation(value = "唯一性校验")
	@GetMapping("/check_unique")
	public Wrapper<Boolean> check_unique(String table, String field, String value) throws Exception {
		boolean b = checkUniqueService.selectTableFindValue(table, field, value);
		return WrapMapper.ok(b);
	}

	@ApiOperation(value = "唯一性校验")
	@GetMapping("/check_unique_notme")
	public Wrapper<Boolean> check_unique_notme(String table, String field, String value, String kid) throws Exception {
		boolean b = checkUniqueService.selectTableFindValueNotme(table, field, value, kid);
		return WrapMapper.ok(b);
	}

}
