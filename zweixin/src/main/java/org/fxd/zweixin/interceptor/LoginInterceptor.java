package org.fxd.zweixin.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fxd.zweixin.exception.BusinessAssert;
import org.fxd.zweixin.exception.ErrorCodeEnum;
import org.fxd.zweixin.util.SessionTool;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 登陆校验
 * 
 * @author 冯晓东 398479251@qq.com
 *
 */
public class LoginInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String userId = SessionTool.getSessionUserId();
		if (userId == null) {
			BusinessAssert.throwError(ErrorCodeEnum.BL403);
			return false;
		}

		return true;
	}

}
