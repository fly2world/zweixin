package org.fxd.zweixin.busi_login.login.domain.dto;

import lombok.Data;

@Data
public class LoginDTO {

	private String uname;
	private String password;
}
