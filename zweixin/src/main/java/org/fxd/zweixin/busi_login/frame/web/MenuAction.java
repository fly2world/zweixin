package org.fxd.zweixin.busi_login.frame.web;

import java.util.List;
import java.util.Map;

import org.fxd.zweixin.busi_login.frame.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tmsps.ne4spring.base.BaseAction;
import org.tmsps.ne4spring.utils.wrapper.WrapMapper;
import org.tmsps.ne4spring.utils.wrapper.Wrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "菜单")
@RestController
public class MenuAction extends BaseAction {

	@Autowired
	private MenuService menuService;

	@ApiOperation(value = "菜单")
	@GetMapping("/menu")
	public Wrapper<List<Map<String, Object>>> menu() {
		List<Map<String, Object>> menu = menuService.loginMenu();
		return WrapMapper.ok(menu);
	}
	
}
