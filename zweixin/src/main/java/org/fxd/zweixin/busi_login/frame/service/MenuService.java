package org.fxd.zweixin.busi_login.frame.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.fxd.zweixin.util.SessionTool;
import org.fxd.zweixin.util.tree.TreeTools;
import org.springframework.stereotype.Service;
import org.tmsps.ne4spring.base.BaseService;
import org.tmsps.ne4spring.utils.ChkTools;

@Service
public class MenuService extends BaseService {

	public List<Map<String, Object>> loginMenu() {
		// TODO 当前登录人员菜单
		String userId = SessionTool.getSessionUserId();
		String sql = "select GROUP_CONCAT(DISTINCT t.code) codes from t_fk_auth t "
				+ " left join t_fk_role t1 on t1.status=0 and find_in_set(t.code, t1.auth_codes)>0 "
				+ " left join t_user_role t2 on t2.status=0 and t2.role_id=t1.kid "
				+ " where t.status=0 and t2.user_id=? order by t.code asc";
		Map<String, Object> map = bs.findMap(sql, userId);
		List<Map<String, Object>> list = null;
		if(ChkTools.isNull(map.get("codes"))) {
			sql = "select t.kid,t.name,t.code,t.name label,t.url component,CONCAT('/',t.code) path,t.icon from t_fk_auth t where t.status=0 order by t.code asc";
			list = bs.findList(sql);
		}else {
			String auth_codes = map.get("codes")+"";
			String[] codes = auth_codes.split(",");
			List<String> all = new ArrayList<>();
			for (String code : codes) {
				all.add(code);
				if(code.length()==3) {
					continue;
				}
				while(code.length()>0) {
					int length = code.length()-3;
					code = code.substring(0, length);
					if(!all.contains(code) && ChkTools.isNotNull(code)) {
						all.add(code);
					}
				}
			}
			sql = "select t.kid,t.name,t.code,t.name label,t.url component,CONCAT('/',t.code) path,t.icon from t_fk_auth t where t.status=0 and find_in_set(t.code, ?)>0 order by t.code asc";
			String string = String.join(",", all.toArray(new String[all.size()]));
			list = bs.findList(sql, string);
		}
		return TreeTools.turnListToTreeForAntd(list);
	}

	
}
