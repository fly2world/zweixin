package org.fxd.zweixin.busi_login.login.domain.dto;

import lombok.Data;

@Data
public class RegisterDTO {

	private String uname;
	private String code;
	private String password;
	private String parent_invent_code;
}
