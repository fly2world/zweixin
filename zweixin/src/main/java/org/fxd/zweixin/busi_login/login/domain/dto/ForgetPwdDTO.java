package org.fxd.zweixin.busi_login.login.domain.dto;

import lombok.Data;

@Data
public class ForgetPwdDTO {

	private String code;// 短信验证码
	private String uname;
	private String password;// 新密码
}
