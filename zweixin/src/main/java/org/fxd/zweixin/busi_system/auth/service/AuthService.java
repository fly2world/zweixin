package org.fxd.zweixin.busi_system.auth.service;

import java.util.List;
import java.util.Map;

import org.fxd.zweixin.busi_system.auth.domian.dto.AuthQuery;
import org.fxd.zweixin.busi_system.auth.domian.po.t_fk_auth;
import org.fxd.zweixin.util.tree.TreeTools;
import org.springframework.stereotype.Service;
import org.tmsps.ne4spring.base.BaseService;
import org.tmsps.ne4spring.orm.param.NeParamList;

@Service
public class AuthService extends BaseService {

	public List<Map<String, Object>> list(AuthQuery authDTO) {
		// TODO 菜单列表
		List<Map<String, Object>> list = this.listAuth(authDTO);
		return TreeTools.turnListToTreeForAntd(list);
	}

	private List<Map<String, Object>> listAuth(AuthQuery authDTO) {
		// TODO 获取全部菜单
		String sql = "select * from t_fk_auth t where t.status=0  and (t.name like ?) and (t.code like ?) order by t.code asc ";
		NeParamList params = NeParamList.makeParams();
		params.addLike(authDTO.getName());
		params.addLike(authDTO.getCode());
		return bs.findList(sql, params);
	}

	public List<Map<String, Object>> lowerList(String auth_id) {
		// TODO 获取下级菜单
		t_fk_auth auth = bs.findById(auth_id, t_fk_auth.class);
		String code = auth.getCode();
		String sql = "select * from t_fk_auth t where t.status=0 and (t.code like ?) and length(t.code)>? order by t.code asc";
		List<Map<String, Object>> list = bs.findList(sql, code + "%", code.length());
		return list;
	}

}
