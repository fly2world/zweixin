package org.fxd.zweixin.busi_system.user.web;

import java.util.Map;

import org.fxd.zweixin.busi_system.user.domian.dto.UserQuery;
import org.fxd.zweixin.busi_system.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tmsps.ne4spring.base.BaseAction;
import org.tmsps.ne4spring.base.ne.NePage;
import org.tmsps.ne4spring.utils.wrapper.WrapMapper;
import org.tmsps.ne4spring.utils.wrapper.Wrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "我的团队")
@RestController
@RequestMapping("/sys/team")
public class TeamAction extends BaseAction {

	@Autowired
	private UserService userService;

	@ApiOperation(value = "全部")
	@GetMapping("/list_data")
	public Wrapper<NePage> list_data(UserQuery userDTO) {
		NePage page = userService.listAllTeam(userDTO);
		return WrapMapper.ok(page);
	}
	
	@ApiOperation(value = "羊之队")
	@GetMapping("/list_sheet")
	public Wrapper<NePage> list_sheet(UserQuery userDTO) {
		NePage page = userService.listSheetTeam(userDTO);
		return WrapMapper.ok(page);
	}

	@ApiOperation(value = "狼之队")
	@GetMapping("/list_wolf")
	public Wrapper<NePage> list_wolf(UserQuery userDTO) {
		NePage page = userService.listWolfTeam(userDTO);
		return WrapMapper.ok(page);
	}
	
	@ApiOperation(value = "下级数量")
	@GetMapping("/count")
	public Wrapper<Map<String, Object>> count(String self_invent_code) {
		Map<String, Object> map = userService.countTeamNum(self_invent_code);
		return WrapMapper.ok(map);
	}

}
