package org.fxd.zweixin.busi_system.user.domian.po;

import java.math.BigDecimal;

import javax.persistence.Table;

import org.tmsps.ne4spring.annotation.PK;
import org.tmsps.ne4spring.orm.model.DataModel;
import org.tmsps.ne4spring.utils.date.CreatedTools;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 人员
 * 
 * @author hxj
 *
 */
@ApiModel(value = "人员")
@Data
@Table
@EqualsAndHashCode(callSuper = false)
public class t_user extends DataModel {

	@ApiModelProperty(value = "账号")
	private String uname;// 作为人员登录账号
	@ApiModelProperty(value = "密码")
	private String pwd;// 密码

	private String parent_invent_code;// 邀请码
	private String self_invent_code;// 邀请码

	private BigDecimal yun_coin;// 当前云币
	private BigDecimal balance_money;// 当前余额

	private BigDecimal lock_yun_coin;// 锁定云币
	private BigDecimal lock_balance_money;// 锁定余额

	private String alipay_uname;// 支付宝账号
	private String wx_uanme;// 微信账号

	private String type = "用户";// 类型 管理员 or 用户
	private String pid;// 身份证

	private String pid_front_file_id;// 身份证正面图片
	private String pid_after_file_id;// 身份证背面图片

	@ApiModelProperty(value = "名字")
	private String name;// 人员真实名字
	@ApiModelProperty(value = "联系电话")
	private String mobile;
	@ApiModelProperty(value = "部门id")
	private String depart_id;// 机构id

	// ========== 系统字段 ========================
	@PK
	private String kid;
	private int status;
	private long created = CreatedTools.getCreated();// 生成时间
}