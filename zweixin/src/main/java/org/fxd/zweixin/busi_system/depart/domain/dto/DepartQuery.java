package org.fxd.zweixin.busi_system.depart.domain.dto;

import org.tmsps.ne4spring.base.dto.BaseEleQuery;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class DepartQuery extends BaseEleQuery {

	private String name;
}
