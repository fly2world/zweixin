package org.fxd.zweixin.busi_system.auth.domian.dto;

import lombok.Data;

@Data
public class AuthQuery {

	private String name;
	private String code;
	private String type = "菜单";
}
