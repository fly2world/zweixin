package org.fxd.zweixin.busi_system.set.web;

import org.fxd.zweixin.busi_system.set.domain.t_fk_setting;
import org.fxd.zweixin.busi_system.set.domain.dto.SettingDTO;
import org.fxd.zweixin.busi_system.set.domain.dto.SettingQuery;
import org.fxd.zweixin.busi_system.set.domain.vo.SettingVO;
import org.fxd.zweixin.busi_system.set.service.SettingService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tmsps.ne4spring.base.BaseAction;
import org.tmsps.ne4spring.base.ne.NePage;
import org.tmsps.ne4spring.utils.wrapper.WrapMapper;
import org.tmsps.ne4spring.utils.wrapper.Wrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "参数设置")
@RestController
@RequestMapping("/sys/set")
public class SettingAction extends BaseAction {

	@Autowired
	private SettingService settingService;

	@ApiOperation(value = "列表")
	@GetMapping("/list")
	public Wrapper<NePage> list(SettingQuery query) {
		NePage page = settingService.listSettings(query);
		return WrapMapper.ok(page);
	}

	@ApiOperation(value = "添加")
	@PostMapping("/add")
	public Wrapper<String> add(@RequestBody SettingDTO settingDTO) {
		t_fk_setting set = new t_fk_setting();
		BeanUtils.copyProperties(settingDTO, set);
		bs.saveObj(set);
		return WrapMapper.ok("保存成功");
	}

	@ApiOperation(value = "修改")
	@PostMapping("/edit")
	public Wrapper<String> edit(@RequestBody SettingDTO settingDTO) {
		t_fk_setting set = bs.findById(settingDTO.getKid(), t_fk_setting.class);
		set.setField(settingDTO.getField());
		set.setVal(settingDTO.getVal());
		set.setNote(settingDTO.getNote());
		set.setType(settingDTO.getType());
		set.setSend_type(settingDTO.getSend_type());
		bs.updateObj(set);
		return WrapMapper.ok("保存成功");
	}

	@ApiOperation(value = "删除")
	@GetMapping("/delete")
	public Wrapper<String> update(String kid) {
		bs.deleteById(kid, t_fk_setting.class);
		return WrapMapper.ok("删除成功");
	}

	@ApiOperation(value = "获取对象")
	@GetMapping("/get")
	public Wrapper<SettingVO> get(String kid) {
		t_fk_setting set = bs.findById(kid, t_fk_setting.class);
		SettingVO setVO = new SettingVO();
		BeanUtils.copyProperties(set, setVO);
		return WrapMapper.ok(setVO);
	}
}
