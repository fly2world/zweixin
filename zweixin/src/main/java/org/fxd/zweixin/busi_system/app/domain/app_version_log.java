package org.fxd.zweixin.busi_system.app.domain;

import org.tmsps.ne4spring.annotation.PK;
import org.tmsps.ne4spring.annotation.Table;
import org.tmsps.ne4spring.orm.model.DataModel;
import org.tmsps.ne4spring.utils.date.CreatedTools;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@ApiModel(value = "app升级记录")
@Data
@Table
@EqualsAndHashCode(callSuper = false)
public class app_version_log extends DataModel {
	
	private String old_version;
	private String new_version;
	private String wgtUrl;
	private String pkgUrl;

	// ========== 系统字段 ========================
	@PK
	private String kid;
	private int status;
	private long created = CreatedTools.getCreated();// 生成时间
}
