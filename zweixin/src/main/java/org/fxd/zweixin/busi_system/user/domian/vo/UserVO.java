package org.fxd.zweixin.busi_system.user.domian.vo;

import lombok.Data;

/**
 * 人员
 * 
 * @author hxj
 *
 */
@Data
public class UserVO {

	private String kid;
	private String uname;// 用户名
	private String name;// 用户姓名
	private String mobile;
	private String role_ids;// 角色 权限id

}
