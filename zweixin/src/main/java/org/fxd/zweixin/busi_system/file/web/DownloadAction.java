package org.fxd.zweixin.busi_system.file.web;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fxd.zweixin.busi_system.file.domain.t_fk_file;
import org.fxd.zweixin.util.ImgTools;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.tmsps.ne4spring.base.BaseAction;
import org.tmsps.ne4spring.utils.ChkTools;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "下载，获取文件")
@Controller
@Scope("prototype")
public class DownloadAction extends BaseAction {

	@Value("${backletter.filepath}")
	private String DATA_PATH;

	@ApiOperation(value = "下载文件")
	@GetMapping("/download/{file_id}")
	public void download(@PathVariable String file_id, HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		if (ChkTools.isNull(file_id)) {
			return;
		}
		t_fk_file file = bs.findById(file_id, t_fk_file.class);
		ServletOutputStream os = resp.getOutputStream();
		FileInputStream fis = null;
		try {

			File img = null;
			if (file != null) {
				img = new File(DATA_PATH + File.separator + file.getFolder_url(), file.getNew_file_name());
				logger.info(img.getCanonicalPath());
			}
			if (img == null || !img.exists()) {
				logger.info("img不存在！");
				img = new File(req.getSession().getServletContext().getRealPath("") + File.separator + "resource"
						+ File.separator + "img", "file404.jpg");
			}
			resp.setContentType(file.getContent_type() + "; charset=utf-8");
			// 设置页面不缓存
			resp.setHeader("Pragma", "No-cache");
			resp.setHeader("Cache-Control", "no-cache");
			if (ChkTools.isNotNull(file)) {
				resp.setHeader("Content-Length", file.getSize() + "");
			} else {
				resp.setHeader("Content-Length", 0 + "");
			}
			resp.setDateHeader("Expires", 0);
			resp.setHeader("content-disposition",
					"attachment;filename=" + URLEncoder.encode(file.getFile_name(), "UTF-8"));

			fis = new FileInputStream(img);
			byte[] b = new byte[10240];
			int len = -1;
			while ((len = fis.read(b)) != -1) {
				os.write(b, 0, len);
			}

		} catch (Exception e) {
			logger.info("文件找不到:" + e);
		} finally {
			os.close();
			if (fis != null) {
				fis.close();
			}
		}
	}
	
	@GetMapping("/img/{file_id}")
	public void downloadys(@PathVariable String file_id, String mw, String mh,HttpServletRequest req, HttpServletResponse resp) throws Exception {
		if (ChkTools.isNull(file_id)) {
			return;
		}
		t_fk_file file = bs.findById(file_id, t_fk_file.class);


		ServletOutputStream os = resp.getOutputStream();
		// 获取保存路径
		try {
			File img = null;
			if (file != null) {
				img = new File(
						DATA_PATH + File.separator + file.getFolder_url() + File.separator + file.getNew_file_name());
			}
			if (img == null || !img.exists()) {
				img = new File(req.getSession().getServletContext().getRealPath("") + File.separator + "resource"
						+ File.separator + "img", "file404.jpg");
			}

			int height = ChkTools.getInteger(mh);
			int width = ChkTools.getInteger(mw);
			height = height < 0 ? 0 : height;
			width = width < 0 ? 0 : width;

			// 设置页面不缓存
			resp.setHeader("Pragma", "No-cache");
			resp.setHeader("Cache-Control", "no-cache");
			resp.setDateHeader("Expires", 0);

			ImgTools.thumbnail_w_h(img, width, height, os);

		} catch (Exception e) {
			logger.info("图片找不到:" + e);
		} finally {
			os.close();
		}
	}
}
