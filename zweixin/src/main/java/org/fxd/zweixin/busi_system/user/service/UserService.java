package org.fxd.zweixin.busi_system.user.service;

import java.util.HashMap;
import java.util.Map;

import org.fxd.zweixin.busi_system.user.domian._role.t_user_role;
import org.fxd.zweixin.busi_system.user.domian.dto.UserDTO;
import org.fxd.zweixin.busi_system.user.domian.dto.UserQuery;
import org.fxd.zweixin.busi_system.user.domian.po.t_user;
import org.fxd.zweixin.busi_system.user.domian.vo.UserVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.tmsps.ne4spring.base.BaseService;
import org.tmsps.ne4spring.base.ne.NePage;
import org.tmsps.ne4spring.orm.param.NeParamList;
import org.tmsps.ne4spring.utils.ChkTools;

@Service
public class UserService extends BaseService {

	public NePage listAdminUsers(UserQuery userDTO) {
		// TODO 列表
		String sql = "select * from t_user t where t.status=0 and (t.name like ?) and (t.uname like ?) and t.type='管理员'  ";
		NeParamList params = NeParamList.makeParams();
		params.addLike(userDTO.getName());
		params.addLike(userDTO.getUname());
		return bs.queryForNePage(sql, params, userDTO.getSort(), userDTO.getPage());
	}

	public NePage list(UserQuery userDTO) {
		// TODO 列表
		String sql = "select * from t_user t where t.status=0 and (t.name like ?) and (t.uname like ?) and t.type='用户'   ";
		NeParamList params = NeParamList.makeParams();
		params.addLike(userDTO.getName());
		params.addLike(userDTO.getUname());
		return bs.queryForNePage(sql, params, userDTO.getSort(), userDTO.getPage());
	}

	@Transactional
	public void saveUser(UserDTO userDTO) {
		// TODO 新增人员
		t_user dp = new t_user();
		BeanUtils.copyProperties(userDTO, dp);
		dp.setType("管理员");
		bs.saveObj(dp);

		String[] ids = userDTO.getRole_ids().split(",");
		for (String id : ids) {
			t_user_role role = new t_user_role();
			role.setUser_id(dp.getKid());
			role.setRole_id(id);
			bs.saveObj(role);
		}
	}

	@Transactional
	public void updateUser(UserDTO userDTO) {
		// TODO 修改人员信息
		t_user userDb = bs.findById(userDTO.getKid(), t_user.class);
		userDb.setName(userDTO.getName());
		userDb.setUname(userDTO.getUname());
		userDb.setMobile(userDTO.getMobile());
		bs.updateObj(userDb);

		// 删除原有权限
		String sql = "delete from t_user_role where status=0 and user_id='" + userDb.getKid() + "' ";
		jt.execute(sql);

		String[] ids = userDTO.getRole_ids().split(",");
		for (String id : ids) {
			t_user_role role = new t_user_role();
			role.setUser_id(userDb.getKid());
			role.setRole_id(id);
			bs.saveObj(role);
		}
	}

	public UserVO getUser(String kid) {
		// TODO 获取对象
		t_user userDb = bs.findById(kid, t_user.class);
		UserVO userVO = new UserVO();
		BeanUtils.copyProperties(userDb, userVO);

		Object role_ids = this.findRoles(kid);
		userVO.setRole_ids(ChkTools.getString(role_ids));
		return userVO;
	}

	private Object findRoles(String user_id) {
		// TODO 获取用户的角色列表
		String sql = "select GROUP_CONCAT(t.role_id) ids from t_user_role t where t.status=0 and (t.user_id=?) ";
		Map<String, Object> map = bs.findMap(sql, user_id);
		return map.get("ids");
	}

	public NePage listAllTeam(UserQuery userDTO) {
		// TODO 所有下级团队
		String sql = "select t.* from ((select * from t_user where status=0 and parent_invent_code=?) "
				+ " union (select t1.* from t_user t inner join t_user t1 on t1.status=0 and "
				+ " t1.parent_invent_code=t.self_invent_code where t.status=0 and t.parent_invent_code=?)) t order by t.created asc";
		String sql_count = "select count(1) cnt from ((select * from t_user where status=0 and parent_invent_code=?) "
				+ " union (select t1.* from t_user t inner join t_user t1 on t1.status=0 and "
				+ " t1.parent_invent_code=t.self_invent_code where t.status=0 and t.parent_invent_code=?)) t ";
		return bs.queryForNePage(sql, sql_count, userDTO.getPage(), userDTO.getSelf_invent_code(),
				userDTO.getSelf_invent_code());
	}

	public NePage listSheetTeam(UserQuery userDTO) {
		// TODO 羊之队
		String sql = "select t.* from ((select * from t_user where status=0 and parent_invent_code=?) "
				+ " union (select t1.* from t_user t inner join t_user t1 on t1.status=0 and "
				+ " t1.parent_invent_code=t.self_invent_code where t.status=0 and t.parent_invent_code=?)) t "
				+ " left join t_user t1 on t.self_invent_code=t1.parent_invent_code and t1.status=0 where t1.kid is null order by t.created asc";
		String sql_count = "select count(1) cnt from ((select * from t_user where status=0 and parent_invent_code=?) "
				+ " union (select t1.* from t_user t inner join t_user t1 on t1.status=0 and "
				+ " t1.parent_invent_code=t.self_invent_code where t.status=0 and t.parent_invent_code=?)) t "
				+ " left join t_user t1 on t.self_invent_code=t1.parent_invent_code and t1.status=0 where t1.kid is null";
		return bs.queryForNePage(sql, sql_count, userDTO.getPage(), userDTO.getSelf_invent_code(),
				userDTO.getSelf_invent_code());
	}

	public NePage listWolfTeam(UserQuery userDTO) {
		// TODO 狼之队
		String sql = "select t.* from ((select * from t_user where status=0 and parent_invent_code=?) "
				+ " union (select t1.* from t_user t inner join t_user t1 on t1.status=0 and "
				+ " t1.parent_invent_code=t.self_invent_code where t.status=0 and t.parent_invent_code=?)) t "
				+ " inner join t_user t1 on t.self_invent_code=t1.parent_invent_code and t1.status=0 order by t.created asc";
		String sql_count = "select count(1) cnt from ((select * from t_user where status=0 and parent_invent_code=?) "
				+ " union (select t1.* from t_user t inner join t_user t1 on t1.status=0 and "
				+ " t1.parent_invent_code=t.self_invent_code where t.status=0 and t.parent_invent_code=?)) t "
				+ " inner join t_user t1 on t.self_invent_code=t1.parent_invent_code and t1.status=0 order by t.created asc";
		return bs.queryForNePage(sql, sql_count, userDTO.getPage(), userDTO.getSelf_invent_code(),
				userDTO.getSelf_invent_code());
	}

	public Map<String, Object> countTeamNum(String self_invent_code) {
		// TODO 下级人员数量
		String first, second;
		String sql = "select count(1) cnt from t_user where status=0 and parent_invent_code=?";
		Map<String, Object> findMap = bs.findMap(sql, self_invent_code);
		if (ChkTools.isNull(findMap)) {
			first = "0";
		} else {
			first = findMap.get("cnt").toString();
		}

		String sql1 = " select t1.* from t_user t inner join t_user t1 on t1.status=0 and "
				+ " t1.parent_invent_code=t.self_invent_code where t.status=0 and t.parent_invent_code=? ";
		Map<String, Object> findMap1 = bs.findMap(sql1, self_invent_code);
		if (ChkTools.isNull(findMap1)) {
			second = "0";
		} else {
			second = findMap.get("cnt").toString();
		}
		Map<String, Object> map = new HashMap<>();
		map.put("first", first);
		map.put("second", second);
		return map;
	}

	public Map<String, Object> getParents(String user_id) {
		// TODO 获取父级，祖级人员
		Map<String, Object> map = new HashMap<>();
		map.put("parent", "");
		map.put("grandParent", "");
		t_user user = bs.findById(user_id, t_user.class);
		if (ChkTools.isNotNull(user.getParent_invent_code())) {
			t_user parent = this.findParent(user.getParent_invent_code());
			map.put("parent", parent.getKid());
			if (ChkTools.isNotNull(parent.getParent_invent_code())) {
				t_user grandParent = this.findParent(parent.getParent_invent_code());
				map.put("grandParent", grandParent.getKid());
			}
		}
		return map;
	}

	private t_user findParent(String self_invent_code) {
		// TODO 通过邀请码获取对象
		String sql = "select * from t_user where status=0 and self_invent_code=?";
		return bs.findObj(t_user.class, sql, self_invent_code);
	}

}
