package org.fxd.zweixin.busi_system.depart.domain.vo;

import lombok.Data;

@Data
public class DepartVO {
	private String name;// 名称
	private String code;// 部门编码 3位1级
	private String note;// 备注
	private String kid;
}
