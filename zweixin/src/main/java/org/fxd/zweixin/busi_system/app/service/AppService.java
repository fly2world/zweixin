package org.fxd.zweixin.busi_system.app.service;

import java.util.List;
import java.util.Map;

import org.fxd.zweixin.busi_system.app.domain.app_version;
import org.springframework.stereotype.Service;
import org.tmsps.ne4spring.base.BaseService;

@Service
public class AppService extends BaseService {

	public app_version getApp() {
		String sql = "select t.* from app_version t where t.status=0 and t.statuz='使用中' and t.type='android' ";
		app_version appversion = bs.findObj(app_version.class, sql);
		return appversion;
	}

	
	public List<Map<String,Object>> list() {
		String sql = "select t.*,t1.file_name from app_version t left join t_fk_file t1 on t.file_id=t1.kid where t.status=0 order by t.created desc ";
		List<Map<String,Object>> list = bs.findList(sql);
		return list;
	}
	
	
	public Map<String,Object> get(String kid) {
		String sql = "select t.*,t1.file_name from app_version t left join t_fk_file t1 on t.file_id=t1.kid where t.status=0 and (t.kid=?) order by t.created desc ";
		Map<String,Object> map = bs.findMap(sql,kid);
		return map;
	}
	
}
