package org.fxd.zweixin.busi_system.auth.domian.po;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.tmsps.ne4spring.annotation.PK;
import org.tmsps.ne4spring.orm.model.DataModel;
import org.tmsps.ne4spring.utils.date.CreatedTools;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 菜单
 * 
 * @author hxj
 *
 */
@Entity
@Data
@Table
@EqualsAndHashCode(callSuper = false)
public class t_fk_auth extends DataModel {

	@Column(length=50)
	private String code;// 编码 每3位一级
	@Column(length=200)
	private String name;
	@Column(length=200)
	private String url;// 地址 跳转页面
	@Column(length=200)
	private String icon;// 图标 class中的值
	@Column(length=50)
	private String type = "菜单";// 类型: 菜单 or 按钮
	@Column(length=50)
	private String system = "管理";// 所属系统
	@Column(length=500)
	private String note;// 备注

	@PK
	@Id
	@Column(length=20)
	private String kid;
	@Column(length=20)
	private int status;
	@Column(length=20)
	private long created = CreatedTools.getCreated();// 生成时间

}
