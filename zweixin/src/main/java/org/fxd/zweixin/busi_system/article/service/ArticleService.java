package org.fxd.zweixin.busi_system.article.service;

import java.util.List;
import java.util.Map;

import org.fxd.zweixin.busi_system.article.domain.ArticleQuery;
import org.springframework.stereotype.Service;
import org.tmsps.ne4spring.base.BaseService;
import org.tmsps.ne4spring.base.ne.NePage;
import org.tmsps.ne4spring.orm.param.NeParamList;

@Service
public class ArticleService extends BaseService {

	public NePage list(ArticleQuery articleQuery) {
		// TODO 列表
		String sql = "select t.*,t1.file_name from t_article t left join t_fk_file t1 on t.file_id=t1.kid where t.status=0  ";
		NeParamList params = NeParamList.makeParams();
		return bs.queryForNePage(sql, params, articleQuery.getSort(), articleQuery.getPage());
	}

	public List<Map<String, Object>> list() {
		// TODO 列表
		String sql = "select t.*,t1.file_name from t_article t left join t_fk_file t1 on t.file_id=t1.kid where t.status=0 and t.statuz='使用中'  ";
		return bs.findList(sql);
	}

	public Map<String, Object> get(String kid) {
		String sql = "select t.*,t1.file_name from t_article t left join t_fk_file t1 on t.file_id=t1.kid where t.status=0 and (t.kid=?) order by t.created desc ";
		Map<String, Object> map = bs.findMap(sql, kid);
		return map;
	}

	public Map<String, Object> find_by_code(String code) {
		String sql = "select t.*,t1.file_name from t_article t left join t_fk_file t1 on t.file_id=t1.kid where t.status=0 and (t.code=?) order by t.created desc ";
		Map<String, Object> map = bs.findMap(sql, code);
		return map;
	}

}
