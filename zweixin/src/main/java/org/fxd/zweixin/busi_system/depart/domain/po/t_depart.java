package org.fxd.zweixin.busi_system.depart.domain.po;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.tmsps.ne4spring.annotation.PK;
import org.tmsps.ne4spring.orm.model.DataModel;
import org.tmsps.ne4spring.utils.date.CreatedTools;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 机构表
 */
@Entity
@Data
@Table(name = "t_depart")
@EqualsAndHashCode(callSuper = false)
public class t_depart extends DataModel {

	@Column(length = 200)
	private String name;// 名称
	@Column(length = 50)
	private String code;// 部门编码 3位1级
	@Column(length = 500)
	private String note;// 备注

	// ========== 系统字段 ========================
	@PK
	@Column(length = 20)
	@Id
	private String kid;
	@Column(length = 20)
	private int status;
	@Column(length = 20)
	private long created = CreatedTools.getCreated();// 生成时间
}