package org.fxd.zweixin.busi_system.user.web;

import javax.validation.Valid;

import org.fxd.zweixin.busi_system.user.domian.dto.ChangePwdDto;
import org.fxd.zweixin.busi_system.user.domian.dto.UserDTO;
import org.fxd.zweixin.busi_system.user.domian.dto.UserQuery;
import org.fxd.zweixin.busi_system.user.domian.po.t_user;
import org.fxd.zweixin.busi_system.user.domian.vo.UserVO;
import org.fxd.zweixin.busi_system.user.service.UserService;
import org.fxd.zweixin.util.SessionTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tmsps.ne4spring.base.BaseAction;
import org.tmsps.ne4spring.base.ne.NePage;
import org.tmsps.ne4spring.utils.wrapper.WrapMapper;
import org.tmsps.ne4spring.utils.wrapper.Wrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "用户管理")
@RestController
@RequestMapping("/sys/user")
public class UserAction extends BaseAction {

	@Autowired
	private UserService userService;

	@ApiOperation(value = "用户列表")
	@GetMapping("/list_admin")
	public Wrapper<NePage> list_admin_page(UserQuery userDTO) {
		NePage page = userService.listAdminUsers(userDTO);
		return WrapMapper.ok(page);
	}
	
	@ApiOperation(value = "用户列表")
	@GetMapping("/list")
	public Wrapper<NePage> list_page(UserQuery userDTO) {
		NePage page = userService.list(userDTO);
		return WrapMapper.ok(page);
	}

	@ApiOperation(value = "新增")
	@PostMapping("/add")
	public Wrapper<String> add(@RequestBody UserDTO userDTO) {
		userService.saveUser(userDTO);
		return WrapMapper.ok("保存成功");
	}

	@ApiOperation(value = "修改")
	@PostMapping("/edit")
	public Wrapper<String> edit(@RequestBody UserDTO userDTO) {
		userService.updateUser(userDTO);
		return WrapMapper.ok("保存成功");
	}

	@ApiOperation(value = "删除")
	@GetMapping("/delete")
	public Wrapper<String> update(String kid) {
		bs.deleteById(kid, t_user.class);
		return WrapMapper.ok("删除成功");
	}

	@ApiOperation(value = "获取对象")
	@GetMapping("/get")
	public Wrapper<UserVO> get(String kid) {
		UserVO userVo = userService.getUser(kid);
		return WrapMapper.ok(userVo);
	}

	@ApiOperation(value = "修改密码")
	@PostMapping("/edit_pwd")
	public Wrapper<String> edit_pwd(@Valid @RequestBody ChangePwdDto changePwdDto) {
		t_user adminPo = bs.findById(SessionTool.getSessionUserId(), t_user.class);
		String pwd = adminPo.getPwd();
		String currentpassword = changePwdDto.getCurrentpwd();
		if (currentpassword.equals(pwd)) {
			adminPo.setPwd(changePwdDto.getNewpwdone());
			bs.updateObj(adminPo);
			return WrapMapper.ok("修改成功");
		} else {
			return WrapMapper.error("密码错误");
		}

	}

}
