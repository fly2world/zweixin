package org.fxd.zweixin.busi_system.file.domain;

import org.tmsps.ne4spring.annotation.FieldType;
import org.tmsps.ne4spring.annotation.NotMap;
import org.tmsps.ne4spring.annotation.PK;
import org.tmsps.ne4spring.annotation.Table;
import org.tmsps.ne4spring.orm.model.DataModel;
import org.tmsps.ne4spring.utils.date.CreatedTools;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Table
@EqualsAndHashCode(callSuper=false)
public class t_fk_file extends DataModel{
	
	private String file_name;
	private String new_file_name;
	private String folder;
	private String folder_url;
	private String content_type;
	private String open_download;// 是否公开下载:是 or 否
	private long size;// 文件大小
	private String note;// 备注

	// ========== 系统字段 ========================

	@PK
	@FieldType
	private String kid;
	private int status;
	private long created = CreatedTools.getCreated();
	
	@NotMap
	private static final long serialVersionUID = 1L;
}
