package org.fxd.zweixin.busi_system.user.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.tmsps.ne4spring.base.BaseService;
import org.tmsps.ne4spring.orm.param.NeParamList;

@Service
public class UserRoleService extends BaseService {
	
	public List<Map<String, Object>> listByRoleId(String role_id) {
		// TODO 获取权限下人员
		String sql = "select * from t_user_role t where t.status=0  and (t.role_id=?) ";
		NeParamList params = NeParamList.makeParams();
		params.addLike(role_id);
		List<Map<String, Object>> list = bs.findList(sql, role_id);
		return list;
	}

}
