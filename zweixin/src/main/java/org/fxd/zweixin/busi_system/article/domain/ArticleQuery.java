package org.fxd.zweixin.busi_system.article.domain;

import org.tmsps.ne4spring.base.dto.BaseEleQuery;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ArticleQuery extends BaseEleQuery {

	private String name;
}
