package org.fxd.zweixin.busi_system.role.service;

import java.util.List;
import java.util.Map;

import org.fxd.zweixin.busi_system.role.domian.dto.RoleQuery;
import org.fxd.zweixin.busi_system.role.domian.po.t_fk_role;
import org.fxd.zweixin.busi_system.user.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tmsps.ne4spring.base.BaseService;
import org.tmsps.ne4spring.base.ne.NePage;
import org.tmsps.ne4spring.orm.param.NeParamList;

@Service
public class RoleService extends BaseService {

	@Autowired
	private UserRoleService userRoleService;

	public NePage list(RoleQuery roleDTO) {
		// TODO 权限列表
		String sql = "select * from t_fk_role t where t.status=0  and (t.name like ?) ";
		NeParamList params = NeParamList.makeParams();
		params.addLike(roleDTO.getName());
		NePage page = bs.queryForNePage(sql, params, roleDTO.getSort(), roleDTO.getPage());
		return page;
	}

	public String delRole(String role_id) {
		// TODO 删除权限
		List<Map<String, Object>> list = userRoleService.listByRoleId(role_id);
		if (!list.isEmpty()) {
			return "该权限下有人员，不能删除";
		}
		bs.deleteById(role_id, t_fk_role.class);
		return "删除成功";
	}

	public List<Map<String, Object>> listRoles() {
		// TODO 角色列表
		String sql = "select * from t_fk_role t where t.status=0 ";
		return bs.findList(sql);
	}
	
	
}
