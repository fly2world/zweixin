package org.fxd.zweixin.busi_system.user.domian.dto;

import org.tmsps.ne4spring.base.dto.BaseAntdQuery;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class UserDTO extends BaseAntdQuery {

	private String kid;
	private String uname;
	private String name;
	private String mobile;
	private String pwd;
	private String depart_id;
	private String role_ids;// 权限id集合
}
