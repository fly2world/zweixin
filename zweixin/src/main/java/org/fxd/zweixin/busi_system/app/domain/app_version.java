package org.fxd.zweixin.busi_system.app.domain;

import javax.persistence.Table;

import org.tmsps.ne4spring.annotation.PK;
import org.tmsps.ne4spring.orm.model.DataModel;
import org.tmsps.ne4spring.utils.date.CreatedTools;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@ApiModel(value = "app版本")
@Data
@Table
@EqualsAndHashCode(callSuper = false)
public class app_version extends DataModel {
	
	private String version;
	private String statuz;  //使用中 or 过期
	private String file_id;
	private String type; //android | ios
	// ========== 系统字段 ========================
	@PK
	private String kid;
	private int status;
	private long created = CreatedTools.getCreated();// 生成时间
}
