package org.fxd.zweixin.busi_system.article.web;

import java.util.List;
import java.util.Map;

import org.fxd.zweixin.busi_system.article.domain.ArticleDTO;
import org.fxd.zweixin.busi_system.article.domain.ArticleQuery;
import org.fxd.zweixin.busi_system.article.domain.t_article;
import org.fxd.zweixin.busi_system.article.service.ArticleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tmsps.ne4spring.base.BaseAction;
import org.tmsps.ne4spring.base.ne.NePage;
import org.tmsps.ne4spring.utils.ChkUtil;
import org.tmsps.ne4spring.utils.wrapper.WrapMapper;
import org.tmsps.ne4spring.utils.wrapper.Wrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "article")
@RestController
@RequestMapping("/article")
public class ArticleAction extends BaseAction {

	@Autowired
	private ArticleService articleService;

	@ApiOperation(value = "文章列表")
	@GetMapping("/article_list")
	public Wrapper<List<Map<String, Object>>> article_list() {
		List<Map<String, Object>> list = articleService.list();
		return WrapMapper.ok(list);
	}

	@ApiOperation(value = "文章列表")
	@GetMapping("/list")
	public Wrapper<NePage> list(ArticleQuery articleQuery) {
		NePage page = articleService.list(articleQuery);
		return WrapMapper.ok(page);
	}

	@ApiOperation(value = "新增")
	@PostMapping("/add")
	public Wrapper<String> add(@RequestBody ArticleDTO articleDTO) {
		t_article article = new t_article();
		BeanUtils.copyProperties(articleDTO, article);
		bs.saveObj(article);
		return WrapMapper.ok("保存成功");
	}

	@ApiOperation(value = "修改")
	@PostMapping("/edit")
	public Wrapper<String> edit(@RequestBody ArticleDTO articleDTO) {
		t_article article = bs.findById(articleDTO.getKid(), t_article.class);
		article.setFile_id(articleDTO.getFile_id());
		article.setStatuz(articleDTO.getStatuz());
		article.setTitle(articleDTO.getTitle());
		article.setNote(articleDTO.getNote());
		article.setCode(articleDTO.getCode());
		bs.updateObj(article);
		return WrapMapper.ok("修改成功");
	}

	@ApiOperation(value = "删除")
	@GetMapping("/delete")
	public Wrapper<String> update(String kid) {
		t_article article = bs.findById(kid, t_article.class);
		article.setStatus(-100);
		bs.updateObj(article);
		return WrapMapper.ok("删除成功");
	}

	@ApiOperation(value = "获取对象")
	@GetMapping("/get")
	public Wrapper<Map<String, Object>> get(String kid) {
		Map<String, Object> map = articleService.get(kid);
		return WrapMapper.ok(map);
	}

	@ApiOperation(value = "根据编号获取文章")
	@GetMapping("/find_by_code")
	public Wrapper<Map<String, Object>> find_by_code(String code) {
		Map<String, Object> map = articleService.find_by_code(code);
		t_article article = bs.findById(map.get("kid"), t_article.class);
		Object num = article.getNum();
		if (ChkUtil.isNotNull(num)) {
			article.setNum(Integer.parseInt(num.toString()) + 1);
			bs.updateObj(article);
		} else {
			article.setNum(1);
			bs.updateObj(article);
		}
		return WrapMapper.ok(map);
	}

}
