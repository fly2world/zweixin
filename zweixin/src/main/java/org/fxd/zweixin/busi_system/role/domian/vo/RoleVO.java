package org.fxd.zweixin.busi_system.role.domian.vo;

import lombok.Data;

@Data
public class RoleVO {

	private String kid;
	private String name;
	private String auth_codes;
	private String note;
	
}
