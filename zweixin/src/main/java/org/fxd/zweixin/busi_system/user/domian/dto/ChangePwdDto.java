package org.fxd.zweixin.busi_system.user.domian.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * @author 冯晓东 398479251@qq.com
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ChangePwdDto {

	@NotBlank
	private String currentpwd;
	@NotBlank
	private String newpwdone;

}
