package org.fxd.zweixin.busi_system.set.domain.dto;
import lombok.Data;


/**
 * 系统参数设置
 */
@Data
public class SettingDTO {

	private String field;// 属性
	private String val;// 值
	private String note;// 备注
	private String type;// 类型 ：  余额  or 云币
	private String send_type;// 类型 ：  定额 or 比例

	private String kid;
}