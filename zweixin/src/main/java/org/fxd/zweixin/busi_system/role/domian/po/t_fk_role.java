package org.fxd.zweixin.busi_system.role.domian.po;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.tmsps.ne4spring.annotation.PK;
import org.tmsps.ne4spring.orm.model.DataModel;
import org.tmsps.ne4spring.utils.date.CreatedTools;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色
 * 
 * @author hxj
 *
 */
@Entity
@Data
@Table(name = "t_fk_role")
@EqualsAndHashCode(callSuper = false)
public class t_fk_role extends DataModel {

	@Column(length = 100)
	private String name;// 角色名称
	@Column(length = 500)
	private String auth_codes;// 权限 t_fk_auth的code集合，逗号分隔
	@Column(length = 500)
	private String note;// 备注

	@PK
	@Id
	@Column(length = 20)
	private String kid;
	@Column(length = 20)
	private int status;
	@Column(length = 20)
	private long created = CreatedTools.getCreated();// 生成时间

}
