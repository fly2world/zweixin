package org.fxd.zweixin.busi_system.user.domian._role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.tmsps.ne4spring.annotation.PK;
import org.tmsps.ne4spring.orm.model.DataModel;
import org.tmsps.ne4spring.utils.date.CreatedTools;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 人员角色表
 * 
 * @author hxj
 *
 */
@Entity
@Data
@Table
@EqualsAndHashCode(callSuper = false)
public class t_user_role extends DataModel {

	@Column(length = 50)
	private String role_id;
	@Column(length = 50)
	private String user_id;
	// ========== 系统字段 ========================
	@PK
	@Id
	@Column(length = 20)
	private String kid;
	@Column(length = 20)
	private int status;
	@Column(length = 20)
	private long created = CreatedTools.getCreated();// 生成时间
}