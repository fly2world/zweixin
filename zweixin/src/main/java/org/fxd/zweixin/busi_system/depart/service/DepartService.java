/**
 * 
 */
package org.fxd.zweixin.busi_system.depart.service;

import java.util.List;
import java.util.Map;

import org.fxd.zweixin.busi_system.depart.domain.dto.DepartQuery;
import org.fxd.zweixin.util.tree.TreeTools;
import org.springframework.stereotype.Service;
import org.tmsps.ne4spring.base.BaseService;
import org.tmsps.ne4spring.orm.param.NeParamList;

@Service
public class DepartService extends BaseService {

	public List<Map<String, Object>> list(DepartQuery queryDTO) {
		// TODO 机构列表
		String sql = "select t.* from t_depart t where t.status=0 and (t.name like ?) order by t.code asc";
		NeParamList params = NeParamList.makeParams();
		params.addLike(queryDTO.getName());
		List<Map<String, Object>> list = bs.findList(sql, params);
		return TreeTools.turnListToTreeForAntd(list);
	}

}
