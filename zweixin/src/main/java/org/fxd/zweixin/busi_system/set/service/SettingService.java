package org.fxd.zweixin.busi_system.set.service;

import org.fxd.zweixin.busi_system.set.domain.t_fk_setting;
import org.fxd.zweixin.busi_system.set.domain.dto.SettingQuery;
import org.springframework.stereotype.Service;
import org.tmsps.ne4spring.base.BaseService;
import org.tmsps.ne4spring.base.ne.NePage;
import org.tmsps.ne4spring.orm.param.NeParamList;

@Service
public class SettingService extends BaseService {

	public NePage listSettings(SettingQuery query) {
		// TODO 查找设置列表
		String sql = "select * from t_fk_setting t where t.status=0 and (t.field like ?) and (t.note like ?) order by t.created asc";
		NeParamList params = new NeParamList();
		params.addLike(query.getField());
		params.addLike(query.getNote());
		return bs.queryForNePage(sql, params, query.getSort(), query.getPage());
	}

	public t_fk_setting findSetting(String type, String field) {
		String sql = "select t.* from t_fk_setting t where t.status=0 and (t.type=?) and (t.field=?) ";
		t_fk_setting setting = bs.findObj(t_fk_setting.class, sql, type, field);
		return setting;
	}

	public String getVal(String field) {
		String sql = "select t.* from t_fk_setting t where t.status=0 and (t.field=?) ";
		t_fk_setting setting = bs.findObj(t_fk_setting.class, sql, field);
		if (setting == null) {
			return null;
		}
		return setting.getVal();
	}

}
