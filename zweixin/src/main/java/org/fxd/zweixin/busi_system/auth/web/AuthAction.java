package org.fxd.zweixin.busi_system.auth.web;

import java.util.List;
import java.util.Map;

import org.fxd.zweixin.busi_system.auth.domian.dto.AuthQuery;
import org.fxd.zweixin.busi_system.auth.domian.po.t_fk_auth;
import org.fxd.zweixin.busi_system.auth.domian.vo.AuthVO;
import org.fxd.zweixin.busi_system.auth.service.AuthService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tmsps.ne4spring.base.BaseAction;
import org.tmsps.ne4spring.utils.wrapper.WrapMapper;
import org.tmsps.ne4spring.utils.wrapper.Wrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "菜单管理")
@RestController
@RequestMapping("/sys/auth")
public class AuthAction extends BaseAction {

	@Autowired
	private AuthService authService;

	@ApiOperation(value = "菜单列表")
	@GetMapping("/list")
	public Wrapper<List<Map<String, Object>>> list_page(AuthQuery authDTO) {
		List<Map<String, Object>> list = authService.list(authDTO);
		return WrapMapper.ok(list);
	}

	@ApiOperation(value = "删除")
	@GetMapping("/delete")
	public Wrapper<String> update(String auth_id) {
		List<Map<String, Object>> list = authService.lowerList(auth_id);
		if (!list.isEmpty()) {
			return WrapMapper.ok("该菜单有下级菜单，不能删除");
		}
		bs.deleteById(auth_id, t_fk_auth.class);
		return WrapMapper.ok("删除成功");
	}

	@ApiOperation(value = "新增")
	@PostMapping("/add")
	public Wrapper<String> add(@RequestBody t_fk_auth authDTO) {
		t_fk_auth dp = new t_fk_auth();
		BeanUtils.copyProperties(authDTO, dp);
		bs.saveObj(dp);
		return WrapMapper.ok("保存成功");
	}

	@ApiOperation(value = "新增")
	@PostMapping("/edit")
	public Wrapper<String> edit(@RequestBody t_fk_auth authDTO) {
		t_fk_auth dp = new t_fk_auth();
		BeanUtils.copyProperties(authDTO, dp);
		t_fk_auth authDb = bs.findById(authDTO.getKid(), t_fk_auth.class);
		dp.setStatus(authDb.getStatus());
		dp.setCreated(authDb.getCreated());
		bs.updateObj(dp);
		return WrapMapper.ok("保存成功");
	}

	@ApiOperation(value = "获取对象")
	@GetMapping("/get")
	public Wrapper<AuthVO> get(String kid) {
		t_fk_auth authDb = bs.findById(kid, t_fk_auth.class);
		AuthVO authVO = new AuthVO();
		BeanUtils.copyProperties(authDb, authVO);
		return WrapMapper.ok(authVO);
	}

}
