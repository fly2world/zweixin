package org.fxd.zweixin.busi_system.role.web;

import java.util.List;
import java.util.Map;

import org.fxd.zweixin.busi_system.role.domian.dto.RoleQuery;
import org.fxd.zweixin.busi_system.role.domian.po.t_fk_role;
import org.fxd.zweixin.busi_system.role.domian.vo.RoleVO;
import org.fxd.zweixin.busi_system.role.service.RoleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tmsps.ne4spring.base.BaseAction;
import org.tmsps.ne4spring.base.ne.NePage;
import org.tmsps.ne4spring.utils.wrapper.WrapMapper;
import org.tmsps.ne4spring.utils.wrapper.Wrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "权限管理")
@RestController
@RequestMapping("/sys/role")
public class RoleAction extends BaseAction {

	@Autowired
	private RoleService roleService;

	@ApiOperation(value = "菜单列表")
	@GetMapping("/list")
	public Wrapper<NePage> list_page(RoleQuery roleDTO) {
		NePage page = roleService.list(roleDTO);
		return WrapMapper.ok(page);
	}

	@ApiOperation(value = "新增")
	@PostMapping("/add")
	public Wrapper<String> add(@RequestBody t_fk_role roleDTO) {
		t_fk_role dp = new t_fk_role();
		BeanUtils.copyProperties(roleDTO, dp);
		bs.saveObj(dp);
		return WrapMapper.ok("保存成功");
	}

	@ApiOperation(value = "修改")
	@PostMapping("/edit")
	public Wrapper<String> edit(@RequestBody t_fk_role roleDTO) {
		t_fk_role dp = new t_fk_role();
		BeanUtils.copyProperties(roleDTO, dp);
		t_fk_role roleDb = bs.findById(roleDTO.getKid(), t_fk_role.class);
		dp.setStatus(roleDb.getStatus());
		dp.setCreated(roleDb.getCreated());
		bs.updateObj(dp);
		return WrapMapper.ok("保存成功");
	}

	@ApiOperation(value = "删除")
	@GetMapping("/delete")
	public Wrapper<String> update(String kid) {
		// 判断权限下是否有人员
		String result = roleService.delRole(kid);
		return WrapMapper.ok(result);
	}

	@ApiOperation(value = "获取对象")
	@GetMapping("/get")
	public Wrapper<RoleVO> get(String kid) {
		t_fk_role roleDb = bs.findById(kid, t_fk_role.class);
		RoleVO roleVO = new RoleVO();
		BeanUtils.copyProperties(roleDb, roleVO);
		return WrapMapper.ok(roleVO);
	}
	
	@ApiOperation(value = "菜单列表")
	@GetMapping("/list_data")
	public Wrapper<List<Map<String, Object>>> list_data() {
		List<Map<String, Object>> list = roleService.listRoles();
		return WrapMapper.ok(list);
	}
}
