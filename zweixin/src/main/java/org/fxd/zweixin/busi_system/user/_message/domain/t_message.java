package org.fxd.zweixin.busi_system.user._message.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.tmsps.ne4spring.annotation.PK;
import org.tmsps.ne4spring.orm.model.DataModel;
import org.tmsps.ne4spring.utils.date.CreatedTools;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@ApiModel(value = "通知")
@Data
@Table(name="t_message")
@EqualsAndHashCode(callSuper = false)
public class t_message extends DataModel {

	@Column(length = 500)
	private String to_user_id;// 通知人员
	@Column(length = 20)
	private String user_id;// 发送人员
	@Column(length = 20)
	private String statuz;// 状态 未读 or 已读

	// ========== 系统字段 ========================
	@PK
	@Id
	@Column(length = 20)
	private String kid;
	@Column(length = 20)
	private int status;
	@Column(length = 20)
	private long created = CreatedTools.getCreated();// 生成时间
}
