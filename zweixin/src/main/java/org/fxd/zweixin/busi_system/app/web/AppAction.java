package org.fxd.zweixin.busi_system.app.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fxd.zweixin.busi_system.app.domain.app_version;
import org.fxd.zweixin.busi_system.app.service.AppService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.tmsps.ne4spring.base.BaseAction;
import org.tmsps.ne4spring.utils.ChkUtil;
import org.tmsps.ne4spring.utils.wrapper.WrapMapper;
import org.tmsps.ne4spring.utils.wrapper.Wrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "app")
@RestController
public class AppAction extends BaseAction {

	@Autowired
	private AppService appService;

	@ApiOperation(value = "检测升级")
	@GetMapping("/check_version")
	public Wrapper<Map<String, Object>> check_version(String name, String version) {
		Map<String, Object> map = new HashMap<String, Object>();
		app_version appversion = appService.getApp();
		String oldversion = appversion.getVersion();
		
		
		int oldv = Integer.parseInt(oldversion.replace(".", ""));
		int newv =  Integer.parseInt(version.replace(".", ""));
		
		if (oldv<=newv) {
			map.put("update", false);
		} else {
			map.put("update", true);
		}
		map.put("file_id", appversion.getFile_id());
		return WrapMapper.ok(map);
	}

	@ApiOperation(value = "app版本列表")
	@GetMapping("/app/list")
	public Wrapper<List<Map<String, Object>>> list() {
		List<Map<String, Object>> list = appService.list();
		return WrapMapper.ok(list);
	}

	@ApiOperation(value = "获取版本")
	@GetMapping("/app/get")
	public Wrapper<Map<String, Object>> get(String kid) {

		Map<String, Object> map = appService.get(kid);

		return WrapMapper.ok(map);
	}

	@ApiOperation(value = " 添加app版本")
	@PostMapping("/app/edit")
	public Wrapper<String> edit(@RequestBody app_version version) {

		app_version app = bs.findById(version.getKid(), app_version.class);
		app.setStatuz(version.getStatuz());
		app.setFile_id(version.getFile_id());
		app.setVersion(version.getVersion());
		app.setType(version.getType());
		bs.updateObj(app);
		return WrapMapper.ok("保存成功!");
	}

	@ApiOperation(value = " 添加app版本")
	@PostMapping("/app/add")
	public Wrapper<String> add(@RequestBody app_version version) {

		app_version app = appService.getApp();
		if (ChkUtil.isNotNull(app)) {
			app.setStatuz("已过期");
			bs.updateObj(app);
		}
		app_version appversion = new app_version();
		BeanUtils.copyProperties(version, appversion);
		bs.saveObj(appversion);
		return WrapMapper.ok("保存成功!");
	}

}
