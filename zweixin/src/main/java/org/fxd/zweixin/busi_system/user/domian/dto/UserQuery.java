package org.fxd.zweixin.busi_system.user.domian.dto;

import org.tmsps.ne4spring.base.dto.BaseEleQuery;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class UserQuery extends BaseEleQuery {

	private String name;
	private String uname;// 用户名
	private String self_invent_code;// 个人邀请码
	private String role_name;
	private String depart_id;
}
