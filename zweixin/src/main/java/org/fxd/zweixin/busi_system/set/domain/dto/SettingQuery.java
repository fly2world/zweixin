package org.fxd.zweixin.busi_system.set.domain.dto;

import org.tmsps.ne4spring.base.dto.BaseEleQuery;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class SettingQuery extends BaseEleQuery {

	private String field;// 属性
	private String note;

}
