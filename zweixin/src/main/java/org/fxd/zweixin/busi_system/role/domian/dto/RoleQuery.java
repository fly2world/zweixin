package org.fxd.zweixin.busi_system.role.domian.dto;

import org.tmsps.ne4spring.base.dto.BaseEleQuery;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class RoleQuery extends BaseEleQuery {

	private String name;
	
}
