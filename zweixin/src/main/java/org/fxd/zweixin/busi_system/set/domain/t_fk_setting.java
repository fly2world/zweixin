package org.fxd.zweixin.busi_system.set.domain;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.tmsps.ne4spring.annotation.PK;
import org.tmsps.ne4spring.annotation.Table;
import org.tmsps.ne4spring.orm.model.DataModel;
import org.tmsps.ne4spring.utils.date.CreatedTools;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 余额/云币参数设置
 */
@Entity
@Data
@Table
@EqualsAndHashCode(callSuper = false)
public class t_fk_setting extends DataModel {

	@Column(length=10)
	private String type;// 类型 ：  余额  or 云币
	@Column(length=10)
	private String send_type;// 类型 ：  定额 or 比例
	@Column(length=10)
	private String field;// 属性
	@Column(length=100)
	private String val;// 值
	private String note;// 备注

	@PK
	@Id
	@Column(length=20)
	private String kid;
	@Column(length=20)
	private int status;
	@Column(length=20)
	private long created = CreatedTools.getCreated();// 生成时间
}