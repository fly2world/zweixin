package org.fxd.zweixin.busi_system.auth.domian.vo;

import lombok.Data;

@Data
public class AuthVO {

	private String kid;
	private String name;
	private String code;// 编码 每3位一级
	private String url;// 地址  跳转页面
	private String icon;// 图标 class中的值
	private String type = "菜单";// 类型: 菜单 or 按钮
	private String system = "管理";// 所属系统
	private String note;
}
