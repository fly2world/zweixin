package org.fxd.zweixin.busi_system.depart.web;

import java.util.List;
import java.util.Map;

import org.fxd.zweixin.busi_system.depart.domain.dto.DepartQuery;
import org.fxd.zweixin.busi_system.depart.domain.po.t_depart;
import org.fxd.zweixin.busi_system.depart.domain.vo.DepartVO;
import org.fxd.zweixin.busi_system.depart.service.DepartService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tmsps.ne4spring.base.BaseAction;
import org.tmsps.ne4spring.utils.wrapper.WrapMapper;
import org.tmsps.ne4spring.utils.wrapper.Wrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "机构管理")
@RestController
@RequestMapping("/sys/depart")
public class DepartAction extends BaseAction {

	@Autowired
	private DepartService departService;

	@ApiOperation(value = "机构列表")
	@GetMapping("/list")
	public Wrapper<List<Map<String, Object>>> list_page(DepartQuery departQuery) {
		List<Map<String, Object>> list = departService.list(departQuery);
		return WrapMapper.ok(list);
	}

	@ApiOperation(value = "删除")
	@GetMapping("/delete")
	public Wrapper<String> update(String kid) {
		bs.deleteById(kid, t_depart.class);
		return WrapMapper.ok("删除成功");
	}

	@ApiOperation(value = "新增")
	@PostMapping("/add")
	public Wrapper<String> add(@RequestBody t_depart orgDTO) {
		t_depart dp = new t_depart();
		BeanUtils.copyProperties(orgDTO, dp);
		bs.saveObj(dp);
		return WrapMapper.ok("保存成功");
	}

	@ApiOperation(value = "新增")
	@PostMapping("/edit")
	public Wrapper<String> edit(@RequestBody t_depart orgDTO) {
		t_depart dp = new t_depart();
		BeanUtils.copyProperties(orgDTO, dp);
		t_depart orgDb = bs.findById(orgDTO.getKid(), t_depart.class);
		dp.setStatus(orgDb.getStatus());
		dp.setCreated(orgDb.getCreated());
		bs.updateObj(dp);
		return WrapMapper.ok("保存成功");
	}

	@ApiOperation(value = "获取对象")
	@GetMapping("/get")
	public Wrapper<DepartVO> get(String kid) {
		t_depart orgDb = bs.findById(kid, t_depart.class);
		DepartVO orgVO = new DepartVO();
		BeanUtils.copyProperties(orgDb, orgVO);
		return WrapMapper.ok(orgVO);
	}

}
