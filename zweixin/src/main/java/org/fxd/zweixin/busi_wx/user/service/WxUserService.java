package org.fxd.zweixin.busi_wx.user.service;

import javax.transaction.Transactional;

import org.fxd.zweixin.busi_wx.user.domain.wx_user;
import org.springframework.stereotype.Service;
import org.tmsps.ne4spring.base.BaseService;
import org.tmsps.ne4spring.utils.JsonUtil;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.api.UserApi;

@Service
public class WxUserService extends BaseService {

	// 根据手机号查询用户
	public wx_user findUserByMobile(String mobile) {
		String sql = "select * from wx_user t where t.sys_mobile=? ";
		wx_user user = bs.findObj(wx_user.class, sql, mobile);
		return user;
	}

	// 绑定手机号
	public wx_user bindMobile(String openid, String mobile) {
		wx_user user = findUserByOpenid(openid);
		user.setSys_mobile(mobile);
		bs.updateObj(user);
		return user;

	}

	public wx_user findUserByOpenid(String openid) {
		String sql = "select * from wx_user t where t.openid=? ";
		wx_user user = bs.findObj(wx_user.class, sql, openid);
		return user;
	}

	// 取消关注
	public void unsubscribe(String openid) {
		wx_user user = findUserByOpenid(openid);
		user.setStatus(-100);
		bs.updateObj(user);

	}

	// 关注
	@Transactional
	public void subscribe(String openid) {
		wx_user user = findUserByOpenid(openid);
		if (user != null) {
			// 存在用户的情况
			if (user.getStatus() != 0) {
				user.setStatus(0);
			}
			// 刷新用户信息
			ApiResult apiResult = UserApi.getUserInfo(openid);
			JSONObject userJson = JsonUtil.jsonStrToJsonObject(apiResult.getJson());
			user.setSubscribe(userJson.getString("subscribe"));
			user.setNickname(userJson.getString("nickname"));
			user.setSex(userJson.getString("sex"));
			user.setCity(userJson.getString("city"));

			user.setCountry(userJson.getString("country"));
			user.setProvince(userJson.getString("province"));
			user.setLanguage(userJson.getString("language"));
			user.setHeadimgurl(userJson.getString("headimgurl"));
			user.setSubscribe_time(userJson.getString("subscribe_time"));

			user.setUnionid(userJson.getString("unionid"));
			user.setRemark(userJson.getString("remark"));
			user.setGroupid(userJson.getString("groupid"));
			user.setTagid_list(userJson.getString("tagid_list"));
			user.setSubscribe_scene(userJson.getString("subscribe_scene"));
			user.setQr_scene(userJson.getString("qr_scene"));
			user.setQr_scene_str(userJson.getString("qr_scene_str"));

			bs.updateObj(user);
			return;
		}
		// 不存在用户的情况
		ApiResult apiResult = UserApi.getUserInfo(openid);
		if (!apiResult.isSucceed()) {
			logger.error(apiResult.toString());
			return;
		}
		JSONObject userJson = JsonUtil.jsonStrToJsonObject(apiResult.getJson());
		wx_user wxUser = JSON.toJavaObject(userJson, wx_user.class);
		bs.saveObj(wxUser);

	}

	// 网页登录的情况
	@Transactional
	public wx_user subscribe(JSONObject userJson) {
		String openid = userJson.getString("openid");
		wx_user user = findUserByOpenid(openid);
		if (user != null) {
			// 存在用户的情况
			// 刷新用户信息
			user.setCity(userJson.getString("city"));
			user.setCountry(userJson.getString("country"));
			user.setHeadimgurl(userJson.getString("headimgurl"));
			user.setLanguage(userJson.getString("language"));
			user.setNickname(userJson.getString("nickname"));
			user.setSex(userJson.getString("sex"));
			user.setProvince(userJson.getString("province"));
			user.setUnionid(userJson.getString("unionid"));

			// 网页获取不到以下信息
			// user.setSubscribe(userJson.getString("subscribe"));
			// user.setSubscribe_time(userJson.getString("subscribe_time"));
			// user.setRemark(userJson.getString("remark"));
			// user.setGroupid(userJson.getString("groupid"));
			// user.setTagid_list(userJson.getString("tagid_list"));
			// user.setSubscribe_scene(userJson.getString("subscribe_scene"));
			// user.setQr_scene(userJson.getString("qr_scene"));
			// user.setQr_scene_str(userJson.getString("qr_scene_str"));

			bs.updateObj(user);
			return user;
		}
		// 不存在用户的情况
		wx_user wxUser = JSON.toJavaObject(userJson, wx_user.class);
		bs.saveObj(wxUser);
		return user;
	}

	// 获取微信总人数
	public long getCountWxUser() {
		String sql = "select count(*) cnt from wx_user t";
		Number cnt = (Number) jt.queryForMap(sql).get("cnt");
		return cnt.longValue();
	}

	// 手机号解绑
	public void unbind(String openid) {
		wx_user wxUser = this.findUserByOpenid(openid);
		wxUser.setSys_code(null);
		wxUser.setSys_mobile(null);
		bs.updateObj(wxUser);

	}

}
