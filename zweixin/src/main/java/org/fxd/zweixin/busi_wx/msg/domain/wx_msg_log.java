package org.fxd.zweixin.busi_wx.msg.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.tmsps.ne4spring.annotation.PK;
import org.tmsps.ne4spring.annotation.Table;
import org.tmsps.ne4spring.orm.model.DataModel;
import org.tmsps.ne4spring.utils.date.CreatedTools;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@ApiModel(value = "微信消息推送记录")
@Data
@Table
@EqualsAndHashCode(callSuper = false)
public class wx_msg_log extends DataModel {

	private String system;
	private String telephone;

	private String first;
	private String keyword1;
	private String keyword2;
	private String keyword3;
	private String keyword4;
	private String keyword5;
	private String keyword6;
	private String remark;

	private String is_success;
	private String send_result;

	// ========== 系统字段 ========================
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@PK
	private String kid;
	private int status;
	private long created = CreatedTools.getCreated();// 生成时间
}
