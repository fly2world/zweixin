package org.fxd.zweixin.busi_wx.msg.domain;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@ApiModel(value = "消息推送接口")
@Data
@EqualsAndHashCode(callSuper = false)
public class TemplateMsgDto {
	@NotBlank
	@ApiModelProperty(value = "first")
	private String first;
	@NotBlank
	@ApiModelProperty(value = "keyword1")
	private String keyword1;
	@NotBlank
	@ApiModelProperty(value = "keyword2")
	private String keyword2;
	@NotBlank
	@ApiModelProperty(value = "keyword3")
	private String keyword3;
	@NotBlank
	@ApiModelProperty(value = "keyword4")
	private String keyword4;
	@ApiModelProperty(value = "keyword5")
	private String keyword5;
	@NotBlank
	@ApiModelProperty(value = "备注")
	private String remark;
}
