package org.fxd.zweixin.busi_wx;

import org.fxd.zweixin.busi_wx.user.domain.wx_user;
import org.fxd.zweixin.busi_wx.user.service.WxUserService;
import org.fxd.zweixin.busi_wx.util.TemplateTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.tmsps.ne4spring.utils.ChkUtil;

import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.msg.in.InTextMsg;
import com.jfinal.weixin.sdk.msg.in.event.InFollowEvent;
import com.jfinal.weixin.sdk.msg.in.event.InMenuEvent;
import com.jfinal.weixin.sdk.msg.out.OutTextMsg;

import net.dreamlu.weixin.annotation.WxMsgController;
import net.dreamlu.weixin.spring.DreamMsgControllerAdapter;

@WxMsgController("/weixin/wx")
public class WeixinController extends DreamMsgControllerAdapter {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private WxUserService wxUserService;

	@Override
	protected void processInFollowEvent(InFollowEvent inFollowEvent) {
		OutTextMsg outMsg = new OutTextMsg(inFollowEvent);
		String openid = inFollowEvent.getFromUserName();

		if (InFollowEvent.EVENT_INFOLLOW_SUBSCRIBE.equals(inFollowEvent.getEvent())) {
			// 关注事件
			outMsg.setContent("感谢您的关注~");
			wxUserService.subscribe(openid);
		}
		if (InFollowEvent.EVENT_INFOLLOW_UNSUBSCRIBE.equals(inFollowEvent.getEvent())) {
			// 取消关注事件
			outMsg.setContent("取消关注.");
			wxUserService.unsubscribe(openid);
		}

		render(outMsg);
	}

	@Override
	protected void processInTextMsg(InTextMsg inTextMsg) {
		String content = inTextMsg.getContent();
		String openid = inTextMsg.getFromUserName();

		if (content == null) {
			content = "";
		}
		String result = "";
		if (content.startsWith("1@")) {
			String mobile = content.substring(2);
			boolean isMobile = ChkUtil.isMobile(mobile);
			if (isMobile == false) {
				result = "手机号不合法.";
			} else {
				wxUserService.bindMobile(openid, mobile);
				result = "您的手机绑定成功.可以发送短信啦.";
			}
		} else if (content.equals("@24678")) {
			wxUserService.unbind(openid);
			result = "您已经解绑工号与手机号.";
		} else if (content.startsWith("2@")) {
			String mobile = content.substring(2);
			boolean isMobile = ChkUtil.isMobile(mobile);
			if (isMobile == false) {
				result = "手机号不合法.";
			} else {
				wx_user user = wxUserService.findUserByMobile(mobile);
				if (user != null) {
					ApiResult sendResult = TemplateTools.sendMsg(user.getOpenid());
					if (sendResult.isSucceed()) {
						result = "发送成功.";
					} else {
						logger.error(sendResult.toString());
						result = "发送失败." + sendResult.getErrorCode() + ":" + sendResult.getErrorMsg();
					}
				} else {
					result = "找不到该手机用户.";
				}
			}

		} else {
			result = "不认识的命令.";
		}

		OutTextMsg outMsg = new OutTextMsg(inTextMsg);
		outMsg.setContent(result);
		render(outMsg);
	}

	@Override
	protected void processInMenuEvent(InMenuEvent inMenuEvent) {
		OutTextMsg outMsg = new OutTextMsg(inMenuEvent);

		String key = inMenuEvent.getEventKey();
		if ("unbind".equals(key)) {
			outMsg.setContent("输入 @24678 为您解绑.");
		} else if ("send".equals(key)) {
			outMsg.setContent("输入 2@手机号 发送消息.\n例如: 2@18699996666\n");
		} else {
			outMsg.setContent("其他菜单消息");
		}

		render(outMsg);
	}

}
