package org.fxd.zweixin.busi_wx.util;

import org.fxd.zweixin.busi_wx.msg.domain.TemplateMsgDto;

import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.api.TemplateData;
import com.jfinal.weixin.sdk.api.TemplateMsgApi;

public class TemplateTools {

	public static ApiResult sendMsg(String openid) {
		String modelId = "Z737cot0DXej2-OXFVpQUGRbOct7RwljLV_rS39-lq8";
		modelId = "Jlawf13BloNyRt_fGodkCOiA0atEV5o2t_NHes9Nxls";
		// String openId = "ozvKov1O6UXZXJkTOv6Cv2D8c2FA";

		// 模板消息，发送测试：pass
		ApiResult result = TemplateMsgApi.send(TemplateData.New()
				// 消息接收者
				.setTouser(openid)
				// 模板id
				.setTemplate_id(modelId)
				// 模板参数
				.add("first", "您有一条待办审批提醒!", "#f00").add("keyword1", "农商行流程银行", "#f99").add("keyword2", "请假申请", "#999")
				.add("keyword3", "张三的请假待审批", "#999").add("keyword4", "张三", "#999").add("keyword5", "待审批", "#999")
				.add("remark", "请您5小时内审核。", "#999").build());

		System.err.println(result);
		return result;
	}

	public static ApiResult sendMsg(String system, String openid, TemplateMsgDto msgDto) {
		String modelId = null;
		if ("OA".equals(system)) {
			modelId = "Z737cot0DXej2-OXFVpQUGRbOct7RwljLV_rS39-lq8";
			modelId = "cTNKxzfInXB6TmlTOTgHGvNBA-T2kSPhPxo2bC08HBc";
		} else if ("BANK".equals(system)) {
			modelId = "Jlawf13BloNyRt_fGodkCOiA0atEV5o2t_NHes9Nxls";
		}

		// 模板消息，发送测试：pass
		ApiResult result = TemplateMsgApi.send(TemplateData.New()
				// 消息接收者
				.setTouser(openid)
				// 模板id
				.setTemplate_id(modelId)
				// 模板参数
				.add("first", msgDto.getFirst(), "#f00").add("keyword1", msgDto.getKeyword1(), "#f99")
				.add("keyword2", msgDto.getKeyword2(), "#999").add("keyword3", msgDto.getKeyword3(), "#999")
				.add("keyword4", msgDto.getKeyword4(), "#999").add("keyword5", msgDto.getKeyword5(), "#999")
				.add("remark", msgDto.getRemark(), "#999").build());

		System.err.println(result);
		return result;
	}

}
