package org.fxd.zweixin.busi_wx.msg.service;

import org.fxd.zweixin.busi_wx.msg.domain.wx_msg_log;
import org.springframework.stereotype.Service;
import org.tmsps.ne4spring.base.BaseService;

@Service
public class WxMsgLogService extends BaseService {

	/**
	 * 记录发送日志
	 * 
	 * @param system
	 * @param telephone
	 * @param first
	 * @param keyword1
	 * @param keyword2
	 * @param keyword3
	 * @param keyword4
	 * @param keyword5
	 * @param remark
	 */
	public void log(String system, String telephone, String first, String keyword1, String keyword2, String keyword3,
			String keyword4, String keyword5, String remark, boolean is_success, String send_result) {
		wx_msg_log log = new wx_msg_log();
		log.setSystem(system);
		log.setTelephone(telephone);
		log.setFirst(first);
		log.setKeyword1(keyword1);
		log.setKeyword2(keyword2);
		log.setKeyword3(keyword3);
		log.setKeyword4(keyword4);
		log.setKeyword5(keyword5);
		log.setKeyword6(null);
		log.setRemark(remark);

		log.setIs_success(is_success + "");
		log.setSend_result(send_result);
		bs.saveObj(log);
	}

}
