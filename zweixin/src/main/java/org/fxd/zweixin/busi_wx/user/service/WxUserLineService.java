package org.fxd.zweixin.busi_wx.user.service;

import java.util.List;
import java.util.Map;

import org.fxd.zweixin.busi_wx.user.domain.wx_user_line;
import org.springframework.stereotype.Service;
import org.tmsps.ne4spring.base.BaseService;
import org.tmsps.ne4spring.utils.ChkUtil;

@Service
public class WxUserLineService extends BaseService {

	// 获取所有连线
	public List<Map<String, Object>> getAllLine() {
		String sql = "select * from wx_user_line t where t.status=0";
		List<Map<String, Object>> list = jt.queryForList(sql);
		return list;
	}

	// 连线
	public wx_user_line line(String sourceId, String targetId) {
		if (ChkUtil.isNull(sourceId) || ChkUtil.isNull(targetId)) {
			return null;
		}
		if (sourceId.equals(targetId)) {
			return null;
		}
		wx_user_line line = getLine(sourceId, targetId);
		if (line == null) {
			line = new wx_user_line();
			line.setSourceID(sourceId);
			line.setTargetID(targetId);
			bs.saveObj(line);
		}
		return line;
	}

	// 获取连线
	public wx_user_line getLine(String sourceId, String targetId) {
		String sql = "select * from wx_user_line t where t.status=0 and t.sourceid=? and t.targetid=?";
		wx_user_line line = bs.findObj(wx_user_line.class, sql, sourceId, targetId);
		return line;
	}

}
