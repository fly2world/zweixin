package org.fxd.zweixin.busi_wx;

import org.fxd.zweixin.busi_wx.msg.domain.TemplateMsgDto;
import org.fxd.zweixin.busi_wx.msg.service.WxMsgLogService;
import org.fxd.zweixin.busi_wx.user.domain.wx_user;
import org.fxd.zweixin.busi_wx.user.service.WxUserService;
import org.fxd.zweixin.busi_wx.util.TemplateTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.tmsps.ne4spring.utils.wrapper.WrapMapper;
import org.tmsps.ne4spring.utils.wrapper.Wrapper;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.api.MenuApi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.dreamlu.weixin.annotation.WxApi;

@Api(description = "微信消息管理")
@WxApi("/weixin/api")
public class WeixinApiController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private WxUserService wxUserService;

	@Autowired
	private WxMsgLogService wxMsgLogService;

	@GetMapping("/cmenu")
	@ResponseBody
	public String cmenu() {
		JSONObject menuJson = new JSONObject();
		JSONArray menuArray = new JSONArray();

		JSONObject btn3 = new JSONObject();
		btn3.put("type", "view");
		btn3.put("name", "号码绑定");
		btn3.put("url", "http://zweixin.77bi.vip/zweixin/wx/bind.html");
		menuArray.add(btn3);

		JSONObject btn2 = new JSONObject();
		btn2.put("name", "正在建设");
		btn2.put("type", "click");
		btn2.put("key", "send");
		menuArray.add(btn2);
		/*
		 * JSONObject btn1 = new JSONObject(); btn1.put("name", "正在建设");
		 * btn1.put("type", "click"); btn1.put("key", "unbind"); menuArray.add(btn1);
		 */
		menuJson.put("button", menuArray);

		ApiResult apiResult = MenuApi.createMenu(menuJson.toJSONString());
		return apiResult.getJson();
	}

	@ApiOperation(value = "消息推送")
	@PostMapping("/push")
	@ResponseBody
	public Wrapper<String> push(String system, String telephone, String first, String keyword1, String keyword2,
			String keyword3, String keyword4, String keyword5, String remark) {

		wx_user user = wxUserService.findUserByMobile(telephone);
		if (user == null) {
			return WrapMapper.error("找不到该手机用户.");
		}
		TemplateMsgDto msgDto = new TemplateMsgDto();
		msgDto.setFirst(first);
		msgDto.setKeyword1(keyword1);
		msgDto.setKeyword2(keyword2);
		msgDto.setKeyword3(keyword3);
		msgDto.setKeyword4(keyword4);
		msgDto.setKeyword5(keyword5);
		msgDto.setRemark(remark);

		if ("OA".equals(system)) {
			// OA系统
		} else if ("BANK".equals(system)) {
			// 验证码
		} else {
			return WrapMapper.error("查无此系统,请修改system参数");
		}

		// 正式发送
		ApiResult sendResult = TemplateTools.sendMsg(system, user.getOpenid(), msgDto);
		if (!sendResult.isSucceed()) {
			logger.error(sendResult.toString());
			// 记录发送日志
			wxMsgLogService.log(system, telephone, first, keyword1, keyword2, keyword3, keyword4, keyword5, remark,
					sendResult.isSucceed(), sendResult.getJson());
			String result = "发送失败." + sendResult.getErrorCode() + ":" + sendResult.getErrorMsg();
			return WrapMapper.error(result);
		}
		// 记录发送日志
		wxMsgLogService.log(system, telephone, first, keyword1, keyword2, keyword3, keyword4, keyword5, remark,
				sendResult.isSucceed(), sendResult.getJson());

		return WrapMapper.ok("发送成功.");

	}

}
