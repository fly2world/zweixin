package org.fxd.zweixin.busi_wx.user.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.tmsps.ne4spring.annotation.PK;
import org.tmsps.ne4spring.annotation.Table;
import org.tmsps.ne4spring.orm.model.DataModel;
import org.tmsps.ne4spring.utils.date.CreatedTools;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@ApiModel(value = "微信用户")
@Data
@Table
@EqualsAndHashCode(callSuper = false)
public class wx_user extends DataModel {

	private String sys_mobile;// 绑定手机号
	private String sys_name;// 用户姓名
	private String sys_code;// 工号

	// 字段参考文档
	// https://developers.weixin.qq.com/doc/offiaccount/User_Management/Get_users_basic_information_UnionID.html#UinonId
	private String subscribe;
	private String openid;
	private String nickname;
	private String sex;

	private String city;
	private String country;
	private String province;
	private String language;
	private String headimgurl;
	private String subscribe_time;

	private String unionid;
	private String remark;
	private String groupid;
	private String tagid_list;
	private String subscribe_scene;
	private String qr_scene;
	private String qr_scene_str;

	// ========== 系统字段 ========================
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@PK
	private String kid;
	private int status;
	private long created = CreatedTools.getCreated();// 生成时间
}
