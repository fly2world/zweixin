package org.fxd.zweixin.busi_wx.user.api;

import org.fxd.zweixin.busi_wx.user.domain.wx_user_line;
import org.fxd.zweixin.busi_wx.user.service.WxUserLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.tmsps.ne4spring.base.BaseAction;
import org.tmsps.ne4spring.utils.wrapper.WrapMapper;
import org.tmsps.ne4spring.utils.wrapper.Wrapper;

import net.dreamlu.weixin.annotation.WxApi;

@WxApi("/weixin/api/user/line")
public class WeixinUserLineApiController extends BaseAction {

	@Autowired
	private WxUserLineService wxUserLineService;

	// 两个用户连线
	@GetMapping("/combineLine")
	@ResponseBody
	public Wrapper<String> combineLine(String originOpenid, String openid) {
		wx_user_line line = wxUserLineService.line(originOpenid, openid);

		if (line == null) {
			return WrapMapper.ok("连线失败.");
		}
		return WrapMapper.ok("连线成功.");
	}

}
