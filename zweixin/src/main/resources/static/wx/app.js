function getServerHttp(path){
    // var domain = "http://zweixin.77bi.vip";
    var domain = "";
    return domain + path;
}
//获取地址栏参数值
function getUrlParam(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); // 构造一个含有目标参数的正则表达式对象
	var r = window.location.search.substr(1).match(reg); // 匹配目标参数
	if (r != null){
		return decodeURIComponent(r[2]);
	}
	return null; //返回参数值
}
//url后面加参数
function combineParam(url,param,val){
    if(url.indexOf(param +"=") > 0 ){
        return url;
    }
    var ch = '?';
    if(url.indexOf('?') >0 ){
        ch = '&';
    }
    return url + ch + param + '=' + val;
}
//净化分享url
function getCleanUrl(){
    var url = window.location.href;
    // 1. 去掉code参数
    var code = getUrlParam("code");
    url = url.replace("code="+code ,"");
    url = url.replace("?&" ,"?");
    url = url.replace("&&" ,"&");
    return url;
}
function wxLogin(){
    var url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=@appid&redirect_uri=@redirect_uri&response_type=code&scope=snsapi_userinfo&state=123#wechat_redirect";
    url = url.replace("@appid",jsConfig.appId);
    url = url.replace("@redirect_uri",encodeURI(window.location.href));
    window.location.href=url;
}
function wxLogin2(){
	var userInfo = null;
    $.ajax({
        type : "GET",
        async: false,
        url : getServerHttp("/zweixin") + "/weixin/api/user/getUserInfo",
        data : {
        	code : getUrlParam("code")
        },
        dateType : 'json',
        success : function(result) {
        	if(result.code == 200){
        		console.log(result.result);
        		userInfo = result.result;
        	}else{
        		alert('系统忙,请联系技术人员,稍后再试!');
        	}
        }
    });
    return userInfo;
}

